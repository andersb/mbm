#!/usr/bin/env python3
"""
    Polarization-space modulation

    Anders Buvarp

    The Bradley Department of Electrical and Computer Engineering
    Virginia Tech

"""

import datetime
import sys
import json
from sys import exit
import torch, numpy
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('Agg')
from mbm_dec import MBM_Net
import scipy.io as spio
import mat73

"""
##############################################################################

    Main entry point for evaluating the MBM neural receiver

##############################################################################
"""

# Version
#print(torch.__version__)

# Time
print(datetime.datetime.now())

# Load config
fd = open('mbm.json')
cfg = json.load(fd)
fd.close()

# Flags
eval_flag = cfg['eval_flag']           # Evaluate the network
sweep_flag = cfg['sweep_flag']         # SNR sweep
plot_flag = cfg['plot_flag']           # Plotting
log_flag = cfg['log_flag']             # Store std out to a log file
tpu_flag = cfg['tpu_flag']             # Run on Colab TPU

# Device
device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")

# The output dimension
n_output = cfg['n_output']

# Hyperparameters
batch_size = cfg['batch_size']
epochs = cfg['epochs']
learning_rate = cfg['lr']
lr = learning_rate
decay = cfg['decay']
act_fn = cfg['act_fn']
act_b = cfg['act_b']
batch_size = cfg['batch_size']
stop_loss = cfg['stop_loss']

# Dimensions
iter_dim = 1
slots_dim = 2
ula_dim = 3
obs_dim = 4          

# Eval data file
fname_eval = cfg['eval_file']
mat_eval = mat73.loadmat(fname_eval)
iq_eval = mat_eval['iq']
lbls_eval = mat_eval['payload']
snr_levels = mat_eval['snr_rng']

# SNR index
first,last = cfg['eval_snr_ix']
eval_snr_ix = np.arange(int(first),int(last)+1)
snr = snr_levels[eval_snr_ix];

# Eval model
eval_model = cfg['eval_model']

n_iter = iq_eval.shape[iter_dim]
n_slots = iq_eval.shape[slots_dim]
n_ula = iq_eval.shape[ula_dim]
n_obs = iq_eval.shape[obs_dim]

print('--------------------')
print('Loaded the eval data ')
print('--------------------')
 
# Set std out
if log_flag == True:

    # Re-direct std out to a log file
    log_file = 'logs/log_iter_%d_slots_%d.txt' % (n_iter,n_slots)
    print('Log is written to %s' % log_file)
    sys.stdout = open(log_file,'w')

# Create the MLP network decoder
dec = MBM_Net(n_iter, n_slots, n_ula, n_obs, n_output, batch_size, act_fn, act_b, stop_loss, device)
dec = dec.double()
dec.to(device)

print('Device:\t\t\t\t',                        device)
print('Learning Rate:\t\t\t',                   lr)
print('SNR Eval Range [dB]:\t\t [%d,%d]' %      (snr[0],snr[-1]))
print('ULA Dimension:\t\t',                     ula_dim)
print('Observations Dimension (first/last):\t', obs_dim)
print('Number of Iterations:\t\t',              n_iter)
print('Number of Slots:\t\t',                   n_slots)
print('Batch Size:\t\t\t',                      batch_size)
print('Activation Function:\t\t',               act_fn)
print('ModReLU b-parameter:\t\t',               act_b)
orate = cfg['outliers']
print('Outlier rate:\t\t',                      orate)

# Check the Eval Network flag
if eval_flag == True:

    # Load model
    dec.load_state_dict(torch.load(eval_model,map_location=torch.device(device)))

    # Test network
    eval_loader = dec.make_loader(iq_eval, lbls_eval, eval_snr_ix, snr_levels, n_iter, n_slots, False)
    ber,ser,acc = dec.accuracy(eval_loader,len(eval_snr_ix))
    print('BER: %0.8e \t SER: %0.8e \t Average Accuracy: %0.2f percent\n' % (ber,ser,acc*100))

# Check for sweeping SNR
if sweep_flag == True:

    # Load model
    dec.load_state_dict(torch.load(eval_model,map_location=torch.device(device)))

    # Compute BER, SER and Accuracy for a range of SNR levels
    ber_list = []
    ser_list = []
    for snr_ix in eval_snr_ix:

        # Generate symbols at a particular SNR value
        sweep_loader = dec.make_loader(iq_eval, lbls_eval, [snr_ix], snr_levels, n_iter, n_slots, False)
        ber,ser,acc = dec.accuracy(sweep_loader,1)
        ber_list.append(ber)
        ser_list.append(ser)
        snr = snr_levels[snr_ix]
        print('SNR: %d \t BER: %.5f \t SER: %.5f \t Accuracy: %0.2f percent' % (snr,ber,ser,acc*100))

    print('BER:',ber_list)
    print('SER:',ser_list)

    # Plot accuracy
    
    leg = 'Epochs: %d   Batch Size: %d' % (epochs,batch_size)
    snr = snr_levels[eval_snr_ix];
    plt.semilogy(snr, ber_list, color = "orange", label=leg)
    plt.xlim(snr[0]-1,snr[-1]+1)
    plt.ylim(10e-4,1)
    plt.grid(True)
    plt.title('RIS Media-Based Modulation with Complex-Valued Neural Network    Bit-Error Rate vs SNR')
    plt.xlabel('SNR [dB]')
    plt.ylabel('BER')
    plt.legend()
    fname_plot = 'plots/mbm_iter_%d_slots_%d_batch_size_%d_epochs_%d_orate_%d.png' % (n_iter,n_slots,batch_size,epochs,orate)
    plt.savefig(fname_plot)
    if plot_flag == True:
        plt.show()

    # Save accuracy to file
    if orate:
    	fname_ber = '../accuracy/ber_%s_orate_%d.txt' % (act_fn,orate)
    	fname_ser = '../accuracy/ser_%s_orate_%d.txt' % (act_fn,orate)
    else:
    	fname_ber = '../accuracy/ber_%s.txt' % act_fn
    	fname_ser = '../accuracy/ser_%s.txt' % act_fn

    np.savetxt(fname_ber,np.asarray(ber_list), delimiter=',')
    np.savetxt(fname_ser,np.asarray(ser_list), delimiter=',')

# Flush to stdout
sys.stdout.flush()
    
# Time
print(datetime.datetime.now())

# EOF
