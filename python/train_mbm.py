#!/usr/bin/env python3
"""
    Polarization-space modulation

    Anders Buvarp

    The Bradley Department of Electrical and Computer Engineering
    Virginia Tech

"""

import datetime
import sys
from sys import exit
import json
from sys import exit
import torch, numpy
import numpy as np
from mbm_dec import MBM_Net
import scipy.io as spio
import mat73

"""
##############################################################################

    Main entry point for training the MBM neural receiver

##############################################################################
"""

# Time
print(datetime.datetime.now())

# Load config
fd = open('mbm.json')
cfg = json.load(fd)
fd.close()

# Flags
plot_flag = cfg['plot_flag']           # Plotting
log_flag = cfg['log_flag']             # Store std out to a log file
tpu_flag = cfg['tpu_flag']             # Run on Colab TPU

# Device
device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
device = torch.device("cpu")

# The output dimension
n_output = cfg['n_output']

# Hyperparameters
batch_size = cfg['batch_size']
epochs = cfg['epochs']
learning_rate = cfg['lr']
lr = learning_rate
decay = cfg['decay']
act_fn = cfg['act_fn']
act_b = cfg['act_b']
batch_size = cfg['batch_size']
stop_loss = cfg['stop_loss']

# Dimensions
iter_dim = 1
slots_dim = 2
ula_dim = 3
obs_dim = 4

# Data file
fname_train = cfg['iq_data_file']

# Load training data
mat_train = mat73.loadmat(fname_train)
iq_train = mat_train['iq']
lbls_train = mat_train['payload']
snr_levels = mat_train['snr_rng']
snr_ix_train = cfg['snr_ix_train']

n_iter = iq_train.shape[iter_dim]
n_slots = iq_train.shape[slots_dim]
n_ula = iq_train.shape[ula_dim]
n_obs = iq_train.shape[obs_dim]

# Model file name
outliers = cfg['outliers']
if outliers:
    fname_model = 'models/mbm_iter_%d_slots_%d_ula_%d_obs_%d_batch_size_%d_epochs_%d_%s_orate_%d_los.ptm' % (n_iter,n_slots,n_ula,n_obs,batch_size,epochs,act_fn,outliers)
else:
    fname_model = 'models/mbm_iter_%d_slots_%d_ula_%d_obs_%d_batch_size_%d_epochs_%d_%s.ptm' % (n_iter,n_slots,n_ula,n_obs,batch_size,epochs,act_fn)

print('------------------------')
print('Loaded the training data ')
print('------------------------')

# Set std out
if log_flag == True:

    # Re-direct std out to a log file
    log_file = 'logs/log_iter_%d_slots_%d.txt' % (n_iter,n_slots)
    print('Log is written to %s' % log_file)
    sys.stdout = open(log_file,'w')

# Create the CNN neural receiver
dec = MBM_Net(n_iter, n_slots, n_ula, n_obs, n_output, batch_size, act_fn, act_b, stop_loss, device)
dec = dec.double()
dec.to(device)
dec.print_set(lr,snr_levels[snr_ix_train],outliers)

# Make the train loaders
train_loader = dec.make_loader(iq_train, lbls_train, snr_ix_train, snr_levels, n_iter, n_slots, True)

# Flush stdout to file
sys.stdout.flush()

# Train the network
dec.train(train_loader, epochs, lr, decay)

# Save MLP to file
torch.save(dec.state_dict(), fname_model)

# Flush to stdout
sys.stdout.flush()
    
# Time
print(datetime.datetime.now())

# EOF
