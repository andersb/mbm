"""
    CNN for decoding media-based modulation (MBM) signals

    Anders Buvarp

    The Bradley Department of Electrical and Computer Engineering
    Virginia Tech
"""

import sys
from sys import exit
import torch
import numpy as np
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from complexLayers import ComplexBatchNorm2d, ComplexConv2d, ComplexLinear
from complexFunctions import complex_relu, complex_max_pool2d, complex_dropout

#
# Three activation functions
#

class MBM_Net(nn.Module):
    def __init__(self, n_iter, n_slots, n_ula, n_obs, n_output, batch_size, act_nm, b, stop_loss, device):
        super(MBM_Net, self).__init__()

        self.bits_per_symbol = 6
        self.n_iter = n_iter
        self.n_slots = n_slots
        self.n_samps = n_iter*n_slots
        self.n_bits = self.n_samps*self.bits_per_symbol
        self.ula_dim = n_ula
        self.obs_dim = n_obs
        self.out_dim = n_output
        self.batch_size = batch_size
        self.act_b = b
        self.act_nm = act_nm
        self.stop_loss = stop_loss
        
        # Pooling size
        self.pool_dim = (1,6)
        
        # Fully connected network dimensions
        fc1_dim = 448
        fc2_dim = int(fc1_dim/2)
        fc3_dim = int(fc1_dim/4)
        self.fc1_dim = fc1_dim
        self.fc2_dim = fc2_dim
        self.fc3_dim = fc3_dim
        
        # Activation function
        if act_nm == 'CReLU':
            self.act_fn = self.CReLU
        elif act_nm == 'zReLU':
            self.act_fn = self.zReLU
        elif act_nm == 'ModReLU':
            self.act_fn = self.ModReLU
        elif act_nm == 'Asinh':
            self.act_fn = self.Asinh
        elif act_nm == 'Casinh':
            self.act_fn = self.Casinh
        elif act_nm == 'Tanh':
            self.act_fn = self.Tanh
        elif act_nm == 'HardTanh':
            self.act_fn = self.HardTanh
        else:
            print('Invalid activation')
            exit()
        
        self.conv1 = ComplexConv2d(1,3,(1,8))
        self.bn  = ComplexBatchNorm2d(3)
        self.conv2 = ComplexConv2d(3,8,(1,8))
		
        self.fc1 = ComplexLinear(fc1_dim,fc2_dim)
        self.fc2 = ComplexLinear(fc2_dim,fc3_dim)
        self.fc3 = ComplexLinear(fc3_dim,n_output)
        self.fcOut = nn.Linear(2*n_output, n_output)
        
        # The processing device; CPU or GPU
        self.device = device
        print('-----------------------')
        print('Created MBM CNN decoder')
        print('-----------------------')
        print('Num samps',self.n_samps)
        print('Num bits',self.n_bits)
		
    def forward(self,xr,xi):
        
        xr,xi = self.conv1(xr,xi)
        xr,xi = self.act_fn(xr,xi)
        xr,xi = complex_max_pool2d(xr,xi,self.pool_dim)
        xr,xi = self.bn(xr,xi)
        
        xr,xi = self.conv2(xr,xi)
        xr,xi = self.act_fn(xr,xi)
        xr,xi = complex_max_pool2d(xr,xi,self.pool_dim)
		
        # Make linear
        xr = xr.view(-1, self.fc1_dim)
        xi = xi.view(-1, self.fc1_dim)
        
        # Fully connected network
        xr,xi = self.fc1(xr,xi)
        xr,xi = self.act_fn(xr,xi)
        #xr,xi = complex_dropout(xr,xi)
        xr,xi = self.fc2(xr,xi)
        xr,xi = self.act_fn(xr,xi)
        xr,xi = self.fc3(xr,xi)
        
        # Complex to real
        re = torch.concatenate([xr,xi],axis=1)
        x = self.fcOut(re)
        return x

    def train(self, trainloader, epochs, lr, decay):
        
        super().train()
        
        # Use cross-entropy for loss function
        criterion = nn.CrossEntropyLoss()
        #loss_f = nn.CrossEntropyLoss(reduction='sum')
        
        # Use the Adam optimizer
        optimizer = optim.Adam(self.parameters(), lr=lr, weight_decay=decay)
        #optimizer = optim.SGD(self.parameters(), lr=lr, momentum=0.9)

        # Number of examples in trainloader
        num = len(trainloader)*self.batch_size
        print('Training with %d examples for %d epochs' % (num,epochs))
        
        # Loss counter
        prev_loss = 1
        loss_counter = 0
        loss_count = 20
        loss_threshold = 0.95
        lr_reduction_fraction = 0.85
 
        # For each epoch
        for epoch in range(epochs):
            running_loss = 0.0
            #sys.stdout.flush()
            for count, data in enumerate(trainloader,0):
                
                # Reset optimizer
                optimizer.zero_grad()
                
                # Extract data from trainloader
                re,im,lbl = data
                
                # Forward propagation
                outp = self(re,im)
                
                # Backward propagation
                loss = criterion(outp,lbl)
                loss.backward()
                optimizer.step()
                running_loss += loss.item()
                
            # Compute average loss
            ave_loss = running_loss / num

            # Compute relative/diff loss
            rel_loss = ave_loss / prev_loss
            di = ave_loss - prev_loss
            prev_loss = ave_loss

            # Update loss counter
            loss_counter += 1

            # Check if we should lower he learning rate
            if loss_counter == loss_count and rel_loss > loss_threshold:
                lr *= lr_reduction_fraction
                optimizer = optim.Adam(self.parameters(), lr=lr, weight_decay=decay)        
                loss_counter = 0

            print('Epoch: %d\tLoss: %.3e\t\tDiff: %.3e\tCount: %d\tLearning Rate: %.5f' % (epoch,ave_loss,di,num,lr))
            #print('Loss Counter: %d\tRel loss change: %e' % (loss_counter,ar))

            if ave_loss < self.stop_loss:
                print('The loss is < %e. Stop training.')
                break

        print('Finished Training')

    #
    # Activation functions
    #

    # Tanh
    def Asinh(self,re,im):
        return torch.asinh(re),torch.asinh(im)
    
    # Tanh
    def Tanh(self,re,im):
        return torch.tanh(re),torch.tanh(im)
    
    # HardTanh
    def HardTanh(self,re,im):
        ht = nn.Hardtanh()
        return ht(re),ht(im)
    
    # CReLU
    def CReLU(self,re,im):
        return torch.relu(re), torch.relu(im)
    
    # zReLU
    def zReLU(self,re,im):
        rmask = torch.ge(re, torch.zeros_like(re)).double()
        imask = torch.ge(im, torch.zeros_like(im)).double()
        mask = rmask * imask
        return re*mask,im*mask

    # ModReLU
    # relu(|z|+b) * (z / |z|)
    # b is bias
    def ModReLU(self,re,im):
        norm = torch.sqrt(re*re+im*im)
        scale = torch.relu(norm + self.act_b) / (norm + 1e-6)
        return re*scale,im*scale

    # Complex-valued hyperbolic arcsin
    def Casinh(self,re,im):
        # Create complex numbers
        z = re+im*1j
        # Hyperbolic arcsin on the complex numbers
        x = torch.arcsinh(z)
        # Extract real/imag
        re = x.real
        im = x.imag
        return re,im

    def accuracy(self, testloader, n_snr):

        # Bit/symbol error
        sum_berr = 0
        sum_serr = 0
        total = 0

        with torch.no_grad():
            for re,im,lbls in testloader:
                
                # To device
                re,im,lbls = re.to(self.device), im.to(self.device), lbls.to(self.device)

                # Forward propagation
                outp = self(re,im)

                # Decode
                _, decoded = torch.max(outp.data,1)

                # Bit errors
                bit_diff = torch.bitwise_xor(lbls,decoded)
                ar = bit_diff.cpu().numpy()
                for x in ar:
                    count = bin(x).count("1")
                    sum_berr += count
                    """
                    print(count)
                    print(bit_diff)
                    print(lbls)
                    print(decoded)
                    exit()
                    """
                    
                # Symbol errors
                sum_serr += (decoded != lbls).sum().item()

                """
                print(decoded[:8])
                print(lbls[:8])
                o = outp.detach().numpy()
                print(o[:4,:])
                exit()
                """
                
        # Compute BER, SER and Accuracy
        ber = sum_berr /  self.n_bits / n_snr
        ser = sum_serr /  self.n_samps / n_snr
        acc = 1 - ser
        return ber,ser,acc
    
    # Generate a dataloader
    def make_loader(self, data_feat, lbls, snr_ix, snr_levels, n_iter, n_slots, shuffle):
        
        # Index to data
        dx = np.arange(0,n_slots)
 
        # Number of SNR values
        num_snr = np.size(snr_ix)
        
        # Extract dimensions
        n = n_iter*n_slots
        self.n_samps = n
        L = num_snr * n
        U = self.ula_dim
        O = self.obs_dim

        # Allocate memory for features and labels
        feat = np.empty((L,U,O),dtype=np.complex_)
        
        # Allocate memory for labels
        labs = np.empty(L)

        # Loop for the snr range
        for six in snr_ix:
            
            # Loop for the iterations
            ix = 0
            for it in range(0,n_iter):

                # Save the labels
                labs[dx] = lbls

                # Copy all slots
                d = data_feat[six,it,:,:,:]
                feat[dx,:,:] = d

                # Update the data index
                dx += n_slots
                ix += 1
                                       
            s = snr_levels[six]
            print("Generated %d examples with SNR %d dB" % (n,s))

        # Append labels to the data
        dlist = []
        for lx in range(L):
            
            # Get samples
            samps = np.array([feat[lx,:,:]])
            re = np.real(samps)
            im = np.imag(samps)
            re = torch.DoubleTensor(re).to(torch.device(self.device))
            im = torch.DoubleTensor(im).to(torch.device(self.device))
            # Get labels
            lbl = np.array(labs[lx])
            lbl = torch.DoubleTensor(lbl).to(torch.device(self.device)).long()
            # Append samples and labels
            dlist.append([re,im,lbl])

        # Make DataLoader
        loader = torch.utils.data.DataLoader(dlist, batch_size=self.batch_size, shuffle=shuffle)
        return loader
    
    # Print parameters
    def print_set(self,lr,train,orate):


        print('Device:\t\t\t\t',                        self.device)
        print('Learning Rate:\t\t\t',                   lr)
        print('SNR Training Levels [dB]:\t',            train)
        #print('SNR Eval Range [dB]:\t\t [%d,%d]' %      (evalu[0],evalu[-1]))
        print('ULA Dimension:\t\t',                     self.ula_dim)
        print('Observations Dimension (first/last):\t', self.obs_dim)
        print('Number of Iterations:\t\t',              self.n_iter)
        print('Number of Slots:\t\t',                   self.n_slots)
        print('Batch Size:\t\t\t',                      self.batch_size)
        print('Activation Function:\t\t',               self.act_nm)
        print('ModReLU b-parameter:\t\t',               self.act_b)
        print('Outlier rate:\t\t\t',                    orate)
        print('Stop loss:\t\t\t',                       self.stop_loss)

    def plot(self,x,y,z,ylabel,zlabel,snr_min,snr_max):
        
        plt.figure(figsize=(10,6))
        plt.scatter(x, y, color = "orange", label=ylabel)
        plt.scatter(x, z, color = "red", label=zlabel)
        plt.title('Alpha vs Alpha Hat    SNR Range: %d dB to %d dB' % (snr_min,snr_max))
        plt.xlabel('Alpha')
        plt.ylabel('Alpha Hat')
        plt.grid(True)
        plt.legend()
        plt.savefig("regr.png")

# EOF
