%
% Geometric median of complex numbers
%
% Anders Buvarp
%
%
% Virginia Tech National Security Institute
%
%
%
% Input:
%
%    cfg            Configuration
%    iq             I/Q samples
%
% Output:
%
%    x              Geometric median
%    cv             Complex-valued median
%    re             The real part
%    im             The imaginary part
%

function [x,cv,re,im] = Weiszfeld(cfg,iq)

% Reshape to row
row = reshape(iq,1,numel(iq));

% Extract real/imag
re = real(row);
im = imag(row);
y = [re;im];

% Initial estimate
x = median(y,2);

% Dump estimate
n_iter = cfg.robust_iter;
est = zeros(2,n_iter+1);

% Loop for n_iter
for ix = 1:n_iter
    % Dump
    est(:,ix) = x;
    % Residuals
    r = x-y;
    % Norms
    nos = vecnorm(r,2,1)';
    % Compute weights
    w = 1 ./ nos;
    % Sum of weights
    sow = sum(w);
    % Weighted mean
    wm = y*w / sow;
    % New estimate
    x = wm;
end % for

% Dump
est(:,n_iter+1) = x;

% Return the geometric median and normalized geometric median
re = x(1);
im = x(2);
cv = re+1j*im;

if cfg.pflag
    plot_Weiszfeld(cfg,est)
end % if 

% EOF