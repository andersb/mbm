%
% Monte Carlo Model for MBM
%
% Anders Buvarp
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%
% Input:
%
%    cfg                        The configuration structure:
%
% Output:
%
%    ber                        Bit-error rate
%    ser                        Quaternions
%    iq                         I/Q
%
function [ber,ser,iq,payload] = robust_mc(cfg)

% Monte-Carlo Configuration
num_iter = cfg.num_iter;
param_type = cfg.param_type;
symb_range = cfg.symb_range;
snr_range = cfg.snr_range;

if cfg.verbose

    %
    % Print Monte-Carlo simulation parameters
    %

    fprintf('\nTransmitter located at:\t\t\t\t(%.3f,%.3f)',cfg.p_tx(1),cfg.p_tx(2))
    fprintf('\nReceiver located at:\t\t\t\t(%.3f,%.3f)',cfg.p_rx(1),cfg.p_rx(2))
    fprintf('\nRIS azimuth:\t\t\t\t%.3f',cfg.ris_azimuth)
    fprintf('\nRIS dimensions:\t\t\t\t%d x %d elements',cfg.E.rows,cfg.E.cols)
    fprintf('\nRIS dimensions:\t\t\t\t%.3f x %.3f meters',cfg.ris_height,cfg.ris_width)
    fprintf('\nRIS spacing, d_y:\t\t\t%.3f [mm]',cfg.d_y*1000)
    fprintf('\nRIS spacing, d_z:\t\t\t%.3f [mm]',cfg.d_z*1000)
    fprintf('\nSNR Min:\t\t\t\t%d',snr_range(1))
    fprintf('\nSNR Max:\t\t\t\t%d',snr_range(end))

    fprintf('\nParam Type:\t\t\t\t%s',param_type)
    fprintf('\nParam Min:\t\t\t\t%.3f',symb_range(1))
    fprintf('\nParam Max:\t\t\t\t%.3f',symb_range(end))
    fprintf('\nSample Rate:\t\t\t\t%.3f [Msamples/s]',cfg.Fs/1e6)
    fprintf('\nSample Period:\t\t\t\t%.3f [nanoseconds]',cfg.Ts*1e9)
    fprintf('\nWavelength:\t\t\t\t%.3f [mm]',cfg.wavelength*1000)
    
    fprintf('\n#######################################################')
    fprintf('\nMonte Carlo with %d iterations for %d slots',num_iter,cfg.num_code_words)
    fprintf('\n#######################################################')
  
end % verbose

% BER and SER
s = length(snr_range);
ber = zeros(s,num_iter);
ser = ber;

% Load I/Q to MATLAB file
% SNR levels x iter x code words x ula x snapshots
load(cfg.iq_fname);

% Payload bits
%payload = pload;
payload_bits = de2bi(payload);

% Store magnitudes and phases
a = length(symb_range);
mags = zeros(1,a);
phs = mags;

% Robust
orate = cfg.outlier_rate;
s = cfg.sigma_impn;
N = cfg.ula_len;

%dump = [];

tic
% Loop for range of SNR
sx = 1;
for snr = snr_range
    
    fprintf('\nSNR: %d dB',snr_range(sx))

    % Monte-Carlo iterations
    for itx = 1:num_iter
        
        fprintf('\nIteration: %d',itx)

        % Loop for symbols
        for symb = symb_range

            % Get baseband data
            bb = squeeze(iq(sx,itx,symb,:,:));

            if 0 
                % Outlier index generation with 1% chance
                p = randi(100,1,cfg.K);
                ox = find(p <= orate);
                cnt = numel(ox);
                
                % Generate noise
                awgn = randn(N,cnt)+1j*randn(N,cnt);
                impn = s*awgn;
                
                % Add impulse noise
                bb(:,ox) = bb(:,ox) + impn;
            end % if 

            % Save I/Q
            if cfg.save_iq_flg
                iq(sx,itx,symb,:,:) = bb;
                continue
            end % if

            if 0
                % Phase
                re = real(bb);
                im = imag(bb);
                at = atan2(im,re);
                phs(symb) = median(median(at,2));
                
                % Magnitude
                ab = abs(bb);
                mags(symb) = median(median(ab,2));
                continue
            end % if 
          
            % Robust baseband estimation
            [~,cv,re,im] = Weiszfeld(cfg,bb);
            if cfg.Sq_flg
                % q-paramter
                q = cfg.q_param;
                % Initial location estimate
                loc0 = cfg.all_ones .* cv;
                % Initial scatter
                scatter = bb*bb'./cfg.ula_len;
                % Sq-estimator of location
                [hat,~,~,~, status] = sEst(bb,'QGAUSS',[],q,loc0,scatter);
                %dump = [dump;d];
                re = median(real(hat));
                im = median(imag(hat));
            end % if
  
            % Phase
            at = atan2(im,re);
            phs(symb) = at;
            
            % Magnitude
            ab = sqrt(re*re+im*im);
            mags(symb) = ab;
                
        end % for each symbol

        % Decode
        data = decode_mbm(cfg,phs,mags);

        % Symbol errors
        ds = data ~= payload;
        serr = sum(sum(ds));
        
        % Bit errors
        db = de2bi(data,cfg.code_word_len) ~= payload_bits;
        berr = sum(sum(db));

        % Save errors
        ber(sx,itx) = berr;
        ser(sx,itx) = serr;

    end % for iter

    % Update the SNR index
    sx = sx+1;

end % for SNR

dur = toc / 60;
fprintf('\n#####################################################')
fprintf('\nThe Monte-Carlo simulation took %.3f minutes',dur)
fprintf('\n#####################################################')
fprintf('\n')

% EOF
