%
% Reconfigurable Intelligent Surfaces for polarization-space modulation
%
% Anders Buvarp
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%
% Input:
%
%   cfg                 Configuration
%   rate                Outlier rate [percent]
%   sigma_imp           Std. dev. of the impulse noise
%   q                   Sq parameter
%   iq_fname            I/Q data file
%
% Output:
%
%   cfg                 Configuration
%

function cfg = robust_config(cfg,outlier_rate,sigma_impn,q,iq_fname)

cfg.outlier_rate = outlier_rate;            % Rate of outliers [percent]
cfg.sigma_impn = sigma_impn;                % Impulse noise std dev
cfg.robust_iter = 8;                        % Number of robust iterations
cfg.q_param = q;                            % The q parameter
cfg.iq_fname = iq_fname;                    % I/Q data file
cfg.all_ones = ones(cfg.ula_len,1);

% EOF
