%
% Set the tunable angles Phi
%
% Anders Buvarp
%
% References:
%
%   Emil Bjornsson, et al., "Intelligent Reflecting Surface Operation Under 
%   Predictable Receiver Mobility: A Continuous Time Propagation Model", 
%   IEEE WIRELESS COMMUNICATIONS LETTERS, VOL. 10, NO. 2, 2021
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%
% Input:
%
%    cfg        Configuration
%
% Output:
%
%    phi        2-D array of angles [radians]
%

function cfg = tunable_delay(cfg)

% Compute the change in phase  by the RISPhi
Phase_max = max(max(cfg.Phase_m_n));
phi = Phase_max - cfg.Phase_m_n;
cfg.phi = mod(phi,2*pi);

% Total phases
cfg.TotPhase = cfg.Phase_m_n + cfg.phi;

% Compute delay associated with Phi
cfg.phi_dly = cfg.phi / cfg.Omega_c;

% RIS path delay
cfg.ris_dly = cfg.Tau_m_n + cfg.phi_dly;            

% Check if we should plot
if cfg.pflag
    
    min(min(cfg.phi))
    max(max(cfg.phi))
    
    s = cfg.E.cols/2;
    y = cfg.d_y*(cfg.rngc-s);
    z = cfg.d_z*((1:cfg.E.rows)-cfg.E.rows/2);
    
    mesh(y,z,cfg.phi/pi)
    
    title('Distribution of Phi Angles over RIS')
    xlabel('Y [meters]')
    ylabel('Z [meters]')
    zlabel('Phi [radians/pi]')
    
end % if

% EOF
