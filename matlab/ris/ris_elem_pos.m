%
% RIS element positions in the X-Y plane
%
% Anders Buvarp
%
% References:
%
% [1] https://arxiv.org/abs/2006.06991
%
% Virginia Tech National Security Institute
%
%
% Input:
%
%   cfg            Configuration
%   P              Rows
%   Q              Columns
%
% Output:
%
%    ris_pos                RIS element positions
%

function cfg = ris_elem_pos(cfg,P,Q)

% Allocate the output
d_y = cfg.d_y;
d_z = cfg.d_z;

% Compute m and n according to function G in Equation 1
m = cfg.rngc-Q/2;
n = cfg.rngr-P/2;

% Compute (y,z) positions of all RIS element
cfg.y_pos_ris = m*d_y - 0.5*d_y*mod(Q+1,2);
cfg.z_pos_ris = n*d_z - 0.5*d_z*mod(P+1,2);

% EOF