%
% RIS init for MBM
%
% Anders Buvarp
%
%   Emil Bjornsson, et al., "Intelligent Reflecting Surface Operation Under 
%   Predictable Receiver Mobility: A Continuous Time Propagation Model", 
%   IEEE WIRELESS COMMUNICATIONS LETTERS, VOL. 10, NO. 2, 2021
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%
%
% Input:
%
%    cfg                    Configuration
%    tx_pos_x               Transmitter position in the X-direction
%    tx_pos_y               Transmitter position in the Y-direction
%    rx_pos_x               Receiver position in the X-direction
%    rx_pos_y               Receiver position in the Y-direction
%    s                      Side dimension of RIS [meters]
%
% Output:
%
%    cfg                    Configuration
%

function cfg = ris_mbm_init(cfg,tx_pos_x,tx_pos_y,rx_pos_x,rx_pos_y,s)

% Geometry
d1 = rx_pos_x - tx_pos_x;
d2 = tx_pos_y - rx_pos_y;
ris_azimuth = atan2(-rx_pos_y,rx_pos_x);   % Azimuth of RIS 
los_azimuth = atan2(d2,d1);                % Azimuth of LOS
cfg.Thetas = [ ris_azimuth los_azimuth ];   % Source directions in radians
cfg.Alphas = [ 1 1 1 ];                     % Source magnitude and phase

% X/Y-range
cfg.x_rng = 1:rx_pos_x+5;
cfg.y_rng = 1:-rx_pos_y;

% Element spacing
cfg.d_y = cfg.wavelength/5;
cfg.d_z = cfg.wavelength/5; 

% Number of rows/columns
cfg.E.rows = round(s/cfg.d_z);
cfg.E.cols = round(s/cfg.d_y);

% Antenna gains
G.tx_rx_dBi  = 0;
G.rx_tx_dBi  = 0;
G.tx_m_n_dBi = 0;                      % Antenna gain [dBi]; Tx in the RIS element direction
G.m_n_tx_dBi = 0;                      % Antenna gain [dBi]; RIS in direction of Tx
G.rx_m_n_dBi = 0;                      % Antenna gain [dBi]; Rx in the RIS element direction
G.m_n_rx_dBi = 0;                      % Antenna gain [dBi]; RIS in direction of Rx
G.mu = 1;                              % The fraction of the incident energy that is scattered
cfg.G = G;

% Geometry
d1 = rx_pos_x - tx_pos_x;
d2 = tx_pos_y - rx_pos_y;
%cfg.ris_azimuth = atan2(rx_pos_x,-rx_pos_y);

% RIS is placed at (x,y,z) = (0,0,0)
% Transmitter is placed on Z-axis at (x,y,z)  = (x_tx,y_tx,0)
% Receiver is placed at (x,y,z) = (x_rx,y_rx,0)
% Tx/Rx vertical position same as RIS, that is, z = 0

% Save the Tx/Rx positions
cfg.p_tx = [tx_pos_x tx_pos_y 0];                    % Tx position
cfg.p_rx = [rx_pos_x rx_pos_y 0];                    % Rx position

% Antenna gains
G = cfg.G;
cfg.G_tx_rx  = 10^(G.tx_rx_dBi/10);   % Linear gain
cfg.G_rx_tx  = 10^(G.rx_tx_dBi/10);   % Linear gain
cfg.G_tx_m_n = 10^(G.tx_m_n_dBi/10);  % Linear gain
cfg.G_rx_m_n = 10^(G.rx_m_n_dBi/10);  % Linear gain
cfg.G_m_n_rx = 10^(G.m_n_rx_dBi/10);  % RIS gain in direction of receiver
cfg.G_m_n_tx = 10^(G.m_n_tx_dBi/10);  % RIS gain in direction of transmitter

% Number of rows and columns in the RIS
num_r = cfg.E.rows;
num_c = cfg.E.cols;

% Range of rows and columns
cfg.rngr = 1:num_r;
cfg.rngc = 1:num_c;

% Height and width of the RIS
cfg.ris_height = num_r * cfg.d_z;
cfg.ris_width = num_c * cfg.d_y;

% Allocate arrays for RIS element positions, attenuations, time delays, 
% phase delays, sample delay offsets, magnitude/phase shifts,
% the programmable phase shifts (Phi) and the delay associated with Phi
cfg.A_m_n               = nan(num_r,num_c);
cfg.Tau_m_n             = nan(num_r,num_c);
cfg.Phase_m_n           = nan(num_r,num_c);
cfg.samp_dly_offset     = nan(num_r,num_c);
cfg.mag_phase_shift     = nan(num_r,num_c);
cfg.phi                 = zeros(num_r,num_c);
cfg.phi_dly             = nan(num_r,num_c);

% RIS element positions
cfg = ris_elem_pos(cfg,num_r,num_c);

% Attenuation and time/phase delay: A(m,n), Tau(m,n), and Phase(m,n)
cfg = ris_amp_dly(cfg);   

% Tunable delay
cfg = tunable_delay(cfg); 

% Slot length
cfg = mbm_slot_len(cfg);

% Compute the magnitude and phase shift
p = cfg.TotPhase;
shift = cfg.A_m_n .* exp(-1j*p);
cfg.mag_phase_shift = shift;

% LOS Amplitude and Delay: A0, Tau0
[A0,Tau0] = los_amp_dly(cfg);                   % Amplitude/Delay at receiver
cfg.A0 = A0;                                    % Amplitude at receiver
cfg.Tau0 = Tau0;                                % LOS delay [seconds]
cfg.Tau0_samps = round(cfg.Fs * Tau0);          % LOS delay [samples]

% Magnitude phase for LOS and RIS
cfg.ris_mp = 1;
cfg.los_mp = A0 * exp(-1j*cfg.Omega_c*Tau0);    % LOS magnitude and phase shift

% EOF