%
% Reconfigurable Intelligent Surfaces for polarization-space modulation
%
% Anders Buvarp
%
% References:
%
%   Emil Bjornsson, et al., "Intelligent Reflecting Surface Operation Under 
%   Predictable Receiver Mobility: A Continuous Time Propagation Model", 
%   IEEE WIRELESS COMMUNICATIONS LETTERS, VOL. 10, NO. 2, 2021
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%
% Input:
%
%       cfg                 Configuration
%       tx_pos_x            Position of transmitter in X-direction
%       tx_pos_y            Position of transmitter in Y-direction
%       rx_pos_x            Position of receiver in X-direction
%       rx_pos_y            Position of receiver in Y-direction
%
% Output:
%
%       res                 Bit error rate / heat map
%       quat                Quaternions
%       symbs               Concatenated channel output
%

function bb = ris_bb(cfg,s,mp,start,kx,nrx,nex)

% Extract RIS samples at time = delay offset + kx
sx = start+kx-1;
ris_samps = s(sx);

% Compute received baseband signal
bb_m_n = mp .* ris_samps;

% Remove elements not used
bb_m_n(nrx,:) = 0;
bb_m_n(end,nex) = 0;

% Add all reflected contributions coherently
bb = sum(sum(bb_m_n));


% EOF
