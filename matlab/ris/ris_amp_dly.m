%
% Attenuation and time/phase delays at the receiver from RIS path
%
% Anders Buvarp
%
% References:
%
%   Emil Bjornsson, et al., "Intelligent Reflecting Surface Operation Under 
%   Predictable Receiver Mobility: A Continuous Time Propagation Model", 
%   IEEE WIRELESS COMMUNICATIONS LETTERS, VOL. 10, NO. 2, 2021
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%
% Input:
%
%    cfg               Configuration
%
% Output:
%
%    A_m_n             Attenuation
%    Tau_m_n           Delay at malicious node
%

function cfg = ris_amp_dly(cfg)

% Distances between RIS elements and tx/rx
d_tx = ris_norm(cfg,cfg.p_tx);
d_rx = ris_norm(cfg,cfg.p_rx);

% Compute the path loss via each RIS element
numer = sqrt(cfg.G.mu)*cfg.wavelength^2;
numer = numer*sqrt(cfg.G_m_n_rx*cfg.G_rx_m_n*cfg.G_tx_m_n*cfg.G_m_n_tx);
denom = 16*pi*pi*d_rx.*d_tx;
cfg.A_m_n = numer ./ denom;

% Compute the time and phase delay at the receiver
tau = (d_rx + d_tx) / cfg.c0;
cfg.Tau_m_n = tau;
cfg.Phase_m_n = cfg.Omega_c * tau;

% EOF