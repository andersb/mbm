%
% Amplitude/delay for the LOS direct path
%
% Anders Buvarp
%
% References:
%
% [1] https://arxiv.org/abs/2006.06991
%
% Virginia Tech National Security Institute
%
%
% Input:
%
%    config     Configuration
%
% Output:
%
%    A          Amplitude at malicious node
%    dly        Delay to the receiver [seconds]
%

function [A,dly] = los_amp_dly(cfg)

% Distance between transmitter and receiver
dist = norm(cfg.p_tx-cfg.p_rx);

% Compute the amplitude at the malicious node from the direct path
numer = cfg.wavelength*sqrt(cfg.G_tx_rx*cfg.G_rx_tx);
denom = 4*pi*dist;
A = numer / denom;

% Compute the delay at the malicious node from the direct path
dly = dist / cfg.c0;

% EOF