%
% Compute the array response vector
%
% Anders Buvarp
%
% References:
%
% [1] https://github.com/dengjunquan/DoA-Estimation-MUSIC-ESPRIT
%
% Virginia Tech National Security Institute
%
%
% Input:
%
%    array_pos      Antenna array positions
%    samp           Received signal sample
%    mp             Magnitude and Phase
%    Theta          Direction of sources
%
% Output:
%
%    h              The array response
%
function h = array_response(array_pos,samp,mp,Theta)

% Length of array
len = length(array_pos);

% Make array of samples
sig = samp * mp * ones(1,len);

% Compute the array response
h = sig .* exp(2j*pi*array_pos.*sin(Theta));

% Return a column vector
h = h.';

% EOF
