%
% Receive K snapshots
%
% Anders Buvarp
%
% References:
%
% [1] https://www.comm.utoronto.ca/~rsadve/Notes/DOA.pdf
%
% Virginia Tech National Security Institute
%
%
% Input:
%
%    cfg        Configuration
%    sig        RIS signal
%    los_sig    LOS signal
%    data       The transmitted data
%
% Output:
%
%    h_out      The realizations (N,K)
%

function h_out = snapshots(cfg,sig,los_sig,data)

% Compute index to rows/elements to ignore
nr = cfg.eisen_ig_rows(data+1);
ne = cfg.eisen_ig_elem(data+1);
nrx = 1:nr;
nex = 1:ne;

% Get the RIS and LOS angles
ris_ang = cfg.Thetas(1);
los_ang = cfg.Thetas(2);
    
% Obtain magnitudes and phases for the RIS
rng = cfg.rngc;
mp = cfg.mag_phase_shift(:,rng);

% Get the index to where MBM starts to operate
start = cfg.los_sample_range(1) - cfg.ris_samp_dly(:,rng);

% Capture the reflections
%refl = [];

% Allocate output
h_out = zeros(cfg.ula_len,cfg.K);

% Loop for K realizations
for kx = 1:cfg.K
    
    % Apply the next sample to the ULA
    %sx = cfg.los_sample_range(kx);
    %bb = los_sig(sx);

    % Get incident response from the LOS angle
    %h = array_response(cfg.array_pos,bb,cfg.los_mp,los_ang);

    % Compute baseband signal from the RIS
    bb = ris_bb(cfg,sig,mp,start,kx,nrx,nex);
    %refl = [ refl bb ];
    
    % Get incident response from the RIS
    ah = array_response(cfg.array_pos,bb,cfg.ris_mp,ris_ang);

    % AWGN
    h_out(:,kx) = ah;
 
end % for

% Check if we plot
if cfg.pflag
    
    % Compute RIS and LOS signals
    los_s = los_mp * sig(1,cfg.los_sample_range);
    los_s = sig(1,cfg.los_sample_range);
    ris_s0 = sig(2,cfg.los_sample_range);
    ris_s1 = sig(3,cfg.los_sample_range);
    
    figure(67)
    plot(abs(los_s),'kx-'),hold,grid
    plot(abs(ris_s0),'bo-')
    plot(abs(ris_s1),'r*-'),hold
    xlabel('Samples')
    ylabel('Magnitude')
    title('The magnitude of the 48 input samples used by the MUSIC algorithm')
    l0 = sprintf('LOS at %.1f degrees',los_ang*180/pi);
    l1 = sprintf('RIS Edge 0 at %.1f Degrees',cfg.Gamma0*180/pi);
    l2 = sprintf('RIS Edge 1 at %.1f Degrees',cfg.Gamma1*180/pi);
    legend(l0,l1,l2)
    
    figure(68)
    plot(angle(los_s)/pi,'kx-'),hold,grid
    plot(angle(ris_s0)/pi,'bo-')
    plot(angle(ris_s1)/pi,'r*-'),hold
    xlabel('Samples')
    ylabel('Phase / PI')
    title('The phase of the 48 input samples used by the MUSIC algorithm')
    l0 = sprintf('LOS at %.1f degrees',los_ang*180/pi);
    l1 = sprintf('RIS Edge 0 at %.1f Degrees',cfg.Gamma0*180/pi);
    l2 = sprintf('RIS Edge 1 at %.1f Degrees',cfg.Gamma1*180/pi);
    legend(l0,l1,l2)
    
end % if

% EOF
