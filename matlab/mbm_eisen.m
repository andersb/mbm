%
% Media-based Modulation with Reconfigurable Intelligent Surfaces
%
% Anders Buvarp
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%
% Input:
%
%   cfg                 Configuration
%
%
% Output:
%
%   cfg                 Configuration
%

function cfg = mbm_eisen(cfg)

% Real part
re0 = 0.5;
re1 = 1;

% Imag part
im0 = sqrt(3);
im1 = im0/2;

% Eisenstein constellation
eis = [

-1 + 1j*5*im1,      53;    % 110101
1j*5*im1,           44;    % 101100
1 + 1j*5*im1,        4;    % 000100

-2.5 + 1j*2*im0,    56;    % 111000
-1.5 + 1j*2*im0,    21;    % 010101
-.5 + 1j*2*im0,     45;    % 101101
 .5 + 1j*2*im0,     36;    % 100100
 1.5 + 1j*2*im0,     2;    % 000010
 2.5 + 1j*2*im0,     1;    % 000001

-3 + 1j*3*im1,      24;    % 011000
-2 + 1j*3*im1,      17;    % 010001
-1 + 1j*3*im1,      37;    % 100101
1j*3*im1,           38;    % 100110
1 + 1j*3*im1,       18;    % 010010
2 + 1j*3*im1,       16;    % 010000
3 + 1j*3*im1,        0;    % 000000

-2.5 + 1j*im0,      49;    % 110001
-1.5 + 1j*im0,      50;    % 110010
-.5 + 1j*im0,       54;    % 110110
 .5 + 1j*im0,       40;    % 101000
 1.5 + 1j*im0,      20;    % 010100
 2.5 + 1j*im0,      32;    % 100000

-3 + 1j*im1,        51;    % 110011
-2 + 1j*im1,        48;    % 110000
-1 + 1j*im1,        52;    % 110100
1j*im1,             55;    % 110111
1 + 1j*im1,          3;    % 000011
2 + 1j*im1,         39;    % 100111
3 + 1j*im1,         33;    % 100001

-2.5,               28;    % 011100
-1.5,               29;    % 011101
-.5,                41;    % 101001
 .5,                57;    % 111001
 1.5,               11;    % 001011
 2.5,               25;    % 011001

-3 - 1j*im1,        30;    % 011110
-2 - 1j*im1,        15;    % 001111
-1 - 1j*im1,        35;    % 100011
-1j*im1,            26;    % 011010
1 - 1j*im1,         10;    % 001010
2 - 1j*im1,         46;    % 101110
3 - 1j*im1,         12;    % 001100

-2.5 - 1j*im0,      31;    % 011111
-1.5 - 1j*im0,      27;    % 011011
-.5 - 1j*im0,       34;    % 100010
 .5 - 1j*im0,        8;    % 001000
 1.5 - 1j*im0,      42;    % 101010
 2.5 - 1j*im0,      14;    % 001110

-3 - 1j*3*im1,      63;    % 111111
-2 - 1j*3*im1,      58;    % 111010
-1 - 1j*3*im1,      22;    % 010110
-1j*3*im1,          13;    % 001103
1 - 1j*3*im1,        9;    % 001001
2 - 1j*3*im1,        5;    % 000101
3 - 1j*3*im1,        6;    % 000110

-2.5 - 1j*2*im0,    62;    % 111110
-1.5 - 1j*2*im0,    60;    % 111101
-.5 - 1j*2*im0,     59;    % 111011
 .5 - 1j*2*im0,     43;    % 101011
 1.5 - 1j*2*im0,    23;    % 010111
 2.5 - 1j*2*im0,     7;    % 000111

-1 - 1j*5*im1,      61;    % 111101
-1j*5*im1,          47;    % 101111
1 - 1j*5*im1,       19;    % 010011

]; % Eisenstein constellation

% Sort the constellation
a = real(eis(:,2));
[~,ex] = sort(a);
eisen = eis(ex,1).';

% Add offset
offset = 0.25+1j*sqrt(3)/4;
cfg.eisen = eisen-offset;

% Save the magnitude and phases
cfg.eisen_mag = abs(cfg.eisen);
cfg.eisen_phase = atan2(imag(cfg.eisen),real(cfg.eisen));

% Save the largest magnitude
cfg.max_mag = max(cfg.eisen_mag);
cfg.min_mag = min(cfg.eisen_mag);

% Relative magnitude
cfg.rel_mag = cfg.eisen_mag / cfg.max_mag;

if cfg.pflag
    figure(10)    
    plot(real(cfg.eisen),imag(cfg.eisen),'o'),grid
    xlim([-5 5])
end % if

% Rows and elements to ignore
[nr,ne] = eisen_table(cfg);
cfg.eisen_ig_rows = nr;
cfg.eisen_ig_elem = ne;

% Old code
if 0
    % Real part
    re = -4:3;
    re0 = re+0.5;
    re1 = -3:3;
    
    % Imag part
    im = -3:2;
    im = sqrt(3)*im;
    im0 = im(2:end);
    im1 = im+sqrt(3)/2;
    
    % Generate the Eisenstein constellation
    eisen = [];
    for rx = 1:numel(re0)
        iq = re0(rx) + 1j*im0;
        eisen = [ eisen iq ];
    end % for
    for rx = 1:numel(re1)
        iq = re1(rx) + 1j*im1;
        eisen = [ eisen iq ];
    end % for
    
    % Trim constellation to 2^6 = 64 points
    remove = [ 1:4 5 36:39 40 41 46 47 52 71 76 77 82 ];
    d = setdiff(1:numel(eisen),remove);
end % if

% EOF
