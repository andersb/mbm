%
% Generate time slot to be transmitted
%
% Anders Buvarp
%
%   Emil Bjornsson, et al., "Intelligent Reflecting Surface Operation Under 
%   Predictable Receiver Mobility: A Continuous Time Propagation Model", 
%   IEEE WIRELESS COMMUNICATIONS LETTERS, VOL. 10, NO. 2, 2021
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%
% Input:
%
%    cfg            Configuration
%    phs            Received phases
%    mags           Received magnitudes
%
% Output:
%
%    data           Decoded data
%

function data = decode_mbm(cfg,phs,mags)

% Allocate output
N = cfg.num_code_words;
data = zeros(1,N);

% Scale factor
mi = min(mags);
ma = max(mags);
scale_mi = cfg.min_mag/mi;
scale_ma = cfg.max_mag/ma;
scale = scale_mi+scale_ma;
scale = scale/2;

%scale = 52100;

% Scale the magnitudes
mg = mags * scale;

% Circular cross-correlation with the phase histogram
%cconv(a,conj(fliplr(b)),7);

% Received constellation
ph = phs;
rx = mg .* exp(1j*ph);

% Decode the data
for dx = 1:N

    % Compute distance to constellation
    dist = abs(cfg.eisen-rx(dx));
    [m,ix] = min(dist);

    % Save received data
    data(dx) = ix-1;
    
end % dx

if cfg.pflag
    plot_mbm_rx(cfg,rx)
end % if

% EOF