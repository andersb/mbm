%
% Media-based Modulation with Reconfigurable Intelligent Surfaces
%
% MATLAB script to configure the RIS Monte Carlo model
%
% Anders Buvarp
%
% References:
%
%   Emil Bjornsson, et al., "Intelligent Reflecting Surface Operation Under 
%   Predictable Receiver Mobility: A Continuous Time Propagation Model", 
%   IEEE WIRELESS COMMUNICATIONS LETTERS, VOL. 10, NO. 2, 2021
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%

clear

% Flags
cfg.verbose = true;
cfg.verbose = false;
cfg.pflag = 0;
cfg.save_src_flg = 0;
cfg.save_iq_flg = 1;
cfg.decode_flg = 0;
cfg.robust_mc_flg = 0;
cfg.robust_flg = 0;
cfg.Sq_flg = 0;

% Eisenstein constellation
cfg = mbm_eisen(cfg);

% ULA config
cfg.ula_len = 8;
ix = 0:cfg.ula_len-1;
cfg.array_pos = ix/2;

% Robust config
orate = 1;                                  % Rate of outliers [percent]
sigma_impn = 10;                            % Impulse noise std dev
q = 0.99;                                      % Sq parameter
%iq_fname = 'iq_slots_512_iter_8_snrs_10.mat';
%iq_fname = 'iq_slots_512_iter_8_snrs_16.mat';
%iq_fname = 'iq_slots_1024_iter_4_snrs_16.mat';
%iq_fname = 'iq_slots_1024_iter_4_snrs_16_orate_1.mat';
iq_fname = '';
%iq_fname = 'iq_slots_512_iter_8_snrs_26_orate_1.mat';
cfg = robust_config(cfg,orate,sigma_impn,q,iq_fname);

% Monte Carlo config
n_iter = 16;                                % Number of Monte-Carlo iterations
n_codewords = 512;                          % Number of code words (512 bytes)
%snr_rng = -15:-6;                          % SNR range
%snr_rng = [1 4 7 10];                      % Training SNR
%snr_rng = 15;
snr_rng = -15:10;                           % SNR range
%snr_rng = [-5 10];
%snr_rng = 10;
num_snapshots = 312;                        % Number of snapshots
cfg = mbm_config(cfg,n_iter,n_codewords,snr_rng,num_snapshots);

% The LOS path samples
start_sample = 745;
cfg.los_sample_range = (1:cfg.K) + start_sample - 1;

% Tx/Rx coordinates  
tx_pos_x = 5;
tx_pos_y = 5;
rx_pos_x = 300;
rx_pos_y = 0;                % Fraunhofer region is 2D^2 / wavelength

% RIS side dimension [meter]
area = 1;
side = sqrt(area);                          

% Init RIS geometry, gains, etc.
cfg = ris_mbm_init(cfg,tx_pos_x,tx_pos_y,rx_pos_x,rx_pos_y,side);

% Payload
m = 2^cfg.code_word_len-1;
payload = randi([0 m],1,n_codewords);
%payload(1:2) = [61 55];
% Testing
if 0
    %tst = [62 62 62 62 38 47];% 17 52 21 56 25];
    %payload(1:numel(tst)) = tst;
    payload = 51*ones(1,n_codewords);
    payload(1:2) = [61 55];
    dx = payload+1;
    cfg.eisen_phase(dx)*180/pi
    cfg.eisen_mag(dx)
end % if

if cfg.save_src_flg 
    savename = sprintf('mbm_labels_snr_range_%d_to_%d.mat',snr_range(1),snr_range(end));
    fname = fullfile('../data', savename);
    save(fname,'payload','snr_rng');
end % if

% Monte Carlo simulations
if cfg.robust_mc_flg
    % Robust estimation
    [res0,res1,iq,pload] = robust_mc(cfg);
else
    % Monte Carlo loop
    [res0,res1,iq] = mbm_mc(cfg,payload);
end % if-else

if ~cfg.save_iq_flg 
    % Compute BER and SER
    num_symb = n_iter*cfg.num_code_words;
    num_bits = num_symb*cfg.code_word_len;
    ber = sum(res0,[2 3]) / num_bits;
    ser = sum(res1,[2 3]) / num_symb;
    
    % Save results
    save ber.mat ber
    save ser.mat ser
else
    % Save I/Q samples to file
    savename = sprintf('iq_slots_%d_iter_%d_snrs_%d_orate_%d.mat',...
                        n_codewords,n_iter,cfg.num_snr,orate);
    fname = fullfile('../data', savename);
    save(fname,'iq','payload','snr_rng','-v7.3');
end % if

% EOF
