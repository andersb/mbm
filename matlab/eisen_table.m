%
% Media-based Modulation with Reconfigurable Intelligent Surfaces
%
% Anders Buvarp
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%
% Input:
%
%   cfg                 Configuration
%
%
% Output:
%
%   cfg                 Configuration
%

function [ig_ro,ig_el] = eisen_table(cfg)

% 650 x 650 element RIS
num_el = 650;

% 64-point constellation
num_b = 6;
d = 0:2^num_b-1;

%bits = de2bi(d,6)';
ph = cfg.eisen_phase;
deg = ph*180/pi;
mag = cfg.eisen_mag;
n = cfg.rel_mag * num_el;
nrows = floor(n);
nel = round((n-nrows)*num_el);

% Hamming distance
N = numel(d);
Ham = zeros(1,N);
for ex = 1:N
    % Index in binary format
    b = de2bi(ex-1,num_b);

    % Next point
    p = cfg.eisen(ex);

    % Compute distance to constellation
    dist = abs(cfg.eisen-p);

    % Find nearest neighbors
    nx = find(dist <= 1);
    
    % Number of neighbors
    % -1 is for self
    nn = numel(nx)-1;

    % Convert to binary
    bn = de2bi(nx-1,num_b);

    % Find differences
    di = sum(sum(b~=bn));

    % Save the average distance
    Ham(ex) = di/nn;
end % dx

% Table
tab = [ d; ph; deg; mag; cfg.rel_mag; nrows; nel; Ham ]';

% Num rows/elements to ignore
ig_ro = num_el-nrows;
ig_el = num_el-nel;
ix = find(ig_ro == 0);
ig_el(ix) = 0;

% EOF
