%
% Compute the required slot length
%
% Anders Buvarp
%
% References:
%
%   Emil Bjornsson, et al., "Intelligent Reflecting Surface Operation Under 
%   Predictable Receiver Mobility: A Continuous Time Propagation Model", 
%   IEEE WIRELESS COMMUNICATIONS LETTERS, VOL. 10, NO. 2, 2021
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%
% Input:
%
%    cfg                Configuration
%
% Output:
%
%    slot_len           The slot length
%    samp_spread        The sample spread
%    samp_dly           The sample offsets for the RIS path delay
%

function cfg = mbm_slot_len(cfg)

% Find the path delay spread
min_dly = min(min(cfg.ris_dly));                % Min RIS delay in seconds
max_dly = max(max(cfg.ris_dly));                % Max RIS delay in seconds
spread = max_dly - min_dly;
cfg.samp_spread = ceil(cfg.Fs*spread);

% Length of time slot in samples
cfg.slot_len = cfg.samp_spread + cfg.K;

% Compute range of samples to use
cfg.sample_range = (1:cfg.K) - 1 + cfg.samp_spread;

% Convert delay to sample offset
dly = cfg.ris_dly - min_dly;
cfg.ris_samp_dly = round(cfg.Fs * dly);

% EOF


