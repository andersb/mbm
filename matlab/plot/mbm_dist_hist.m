%
% Media-based Modulation with Reconfigurable Intelligent Surfaces
%
% Anders Buvarp
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%
%
% Input:
%
%    d            The squared Mahalanobis distances
%
function mbm_dist_hist(d)

% Font and marker sizes
font_size = 45;
marker_size = 1;
line_width = 4;
bins = 250;
n = numel(d);

ix = find(d < 5e-7);
ox = find(d >= 5e-7);

% Histogram
f55 = figure(55);
%set(f55,'defaulttextinterpreter','latex');

set(0, 'DefaultTextFontName', 'Calibri', 'DefaultTextFontSize', font_size, ...
       'DefaultAxesFontName', 'Calibri', 'DefaultAxesFontSize', font_size); 

histogram(d,bins,'EdgeColor','auto','FaceColor','auto'),grid,hold
histogram(d(ox),bins,'EdgeColor','red','FaceColor','red'),hold
ylabel('Count')
xlabel('Squared Mahalanobis Distance')
str = sprintf('%d distances',n);
stro = sprintf('%d outliers',numel(ox));
legend(str,stro)

% EOF
