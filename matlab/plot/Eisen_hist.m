%
% Media-based Modulation with Reconfigurable Intelligent Surfaces
%
% Anders Buvarp
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%
%
% Input:
%
%    cfg            The configuration
%
function mbm_hist(cfg)

% Font and marker sizes
font_size = 40;
marker_size = 1;
line_width = 4;
bar_width = .2;
bins = 64;

% Phases
th = atan2(imag(cfg.eisen),real(cfg.eisen));
un_th = numel(unique(th));

% Magnitudes
mag = abs(cfg.eisen);
un_mag = numel(unique(mag));

% Histogram
f77 = figure(77);
set(f77,'defaulttextinterpreter','latex');

set(0, 'DefaultTextFontName', 'Calibri', 'DefaultTextFontSize', font_size, ...
       'DefaultAxesFontName', 'Calibri', 'DefaultAxesFontSize', font_size); 

subplot(2,1,1)
h = histogram(th,bins);
ylabel('Count')
axis([-pi pi 0 6]);
xlabel('Phase Angles [rads]')
str = sprintf('%d Unique Phase Angles',un_th);
legend(str)
grid

subplot(2,1,2)
h = histogram(mag,bins);
ylabel('Count')
axis([0 5 0 6]);
xlabel('Magnitudes')
str = sprintf('%d Unique Magnitudes',un_mag);
legend(str)
grid

% Re-size
%set(gcf,'units','points','position',[100,100,500,300])

% EOF
