%
% Media-based Modulation with Reconfigurable Intelligent Surfaces
%
% Anders Buvarp
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%
%
% Input:
%
%    cfg            The configuration
%
function plot_Weiszfeld(cfg,est)

% Font and marker sizes
font_size = 30;
marker_size = 10;
line_width = 4;

set(0, 'DefaultTextFontName', 'Calibri', 'DefaultTextFontSize', font_size, ...
       'DefaultAxesFontName', 'Calibri', 'DefaultAxesFontSize', font_size); 

f66 = figure(66);
set(f66,'defaulttextinterpreter','latex');

% Normalize
s = abs(est(1));
%est = est / s;

subplot(211)
plot(est(1,:),'LineWidth',line_width,'MarkerSize',marker_size),grid
ylabel('real')
legend('Initialize with median of real/imag','Initialize with median of magnitude/phase')

subplot(212)
plot(est(2,:),'LineWidth',line_width,'MarkerSize',marker_size),grid
xlabel('Weiszfeld Iteration')
ylabel('imag')
legend('Initialize with median of real/imag','Initialize with median of magnitude/phase')

% EOF
