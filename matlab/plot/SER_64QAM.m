M = 64; % Alphabet size
P = 8;
bitsPerSym = log2(M);
N = 60000; % Number of bits
x = randi([0 P-1],N,1);
txSig = pskmod(x,P);
x = randi([0 M-1],N,1);
txSig = qammod(x,M);
snr_range = -15:10;
ser = [];
for snr = snr_range%(end)
    rxSig = awgn(txSig,snr);
    %cd = comm.ConstellationDiagram(ShowReferenceConstellation=false);
    %cd(rxSig)
    z = pskdemod(rxSig,P);
    z = qamdemod(rxSig,M);
    ser = [ser sum(sum(x~=z))/N];
    %[num,rt]= symerr(x,z);
end % SNR

semilogy(snr_range,ser);grid;
title('Symbol error rate for 64-QAM over AWGN');
xlabel('SNR [dB]');
ylabel('SER');