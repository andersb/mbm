%
% Plot the GM iterations
%
% Anders Buvarp
%
% Virginia Tech National Security Institute
%
%
%
% Input:
%
%    rx             The received quaternions
%
function plot_iter(est)

% Font and marker sizes
font_size = 40;
marker_size = 1;
line_width = 4;

f66 = figure(66)
set(0, 'DefaultTextFontName', 'Calibri', 'DefaultTextFontSize', font_size, ...
       'DefaultAxesFontName', 'Calibri', 'DefaultAxesFontSize', font_size); 

set(f66,'defaulttextinterpreter','latex');
plot(est(1,:),'LineWidth',line_width,'MarkerSize',marker_size)
grid,hold
plot(est(2,:),'LineWidth',line_width,'MarkerSize',marker_size)
plot(est(3,:),'LineWidth',line_width,'MarkerSize',marker_size)
plot(est(4,:),'LineWidth',line_width,'MarkerSize',marker_size),
hold
xlabel('iteration')
ylabel('Quaternion Estimate, $\hat q$')
legend('a','b','c','d')

% EOF
