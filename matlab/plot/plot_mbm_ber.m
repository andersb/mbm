%
% Plot bit error rate (BER) for media-based modulation
%
% Anders Buvarp
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%

% Load data
fdCReLU = fopen('../accuracy/ber_CReLU.txt','r');
fdModReLUb02 = fopen('../accuracy/ber_ModReLU_b_02.txt','r');
fdModReLUb05 = fopen('../accuracy/ber_ModReLU_b_05.txt','r');
fdModReLUb08 = fopen('../accuracy/ber_ModReLU_b_08.txt','r');
fdAsinh = fopen('../accuracy/ber_Asinh.txt','r');
fdCasinh = fopen('../accuracy/ber_Casinh.txt','r');
fdTanh = fopen('../accuracy/ber_Tanh.txt','r');
fdHardTanh = fopen('../accuracy/ber_HardTanh.txt','r');

% Read BER data from files
ber_CReLU = fscanf(fdCReLU,'%f');
ber_ModReLU_b_02 = fscanf(fdModReLUb02,'%f');
ber_ModReLU_b_05 = fscanf(fdModReLUb05,'%f');
ber_ModReLU_b_08 = fscanf(fdModReLUb08,'%f');
ber_Asinh = fscanf(fdAsinh,'%f');
ber_Casinh = fscanf(fdCasinh,'%f');
ber_Tanh = fscanf(fdTanh,'%f');
ber_HardTanh = fscanf(fdHardTanh,'%f');

fclose(fdCReLU);fclose(fdModReLUb02);fclose(fdModReLUb05);fclose(fdModReLUb08);
fclose(fdAsinh);fclose(fdCasinh);fclose(fdTanh);fclose(fdHardTanh);

% 64-QAM BER
ber_64_qam = [0.34008 0.32001 0.29911 0.27818 0.25973 0.23382 0.21216 ...
              0.19096 0.16886 0.14833 0.12893 0.11133 0.093542 0.075842 ...
              0.060883 0.045742 0.032667 0.022542 0.013067 0.0076333 ...
              0.0035167 0.0013933 0.00048 0.00010083 1.9167e-05 2.5e-06];

% 8-PSK BER
ber_8_psk = [0.45451 0.44881 0.44133 0.43501 0.42536 0.41424 0.40597  0.3952 0.38244 ...
            0.36495 0.35065 0.33008 0.31121 0.29021 0.26687 0.24135 0.21675 0.19019 ...
            0.16411 0.14147 0.11777  0.0976 0.076992 0.059225 0.04255 0.028958];

% Arctan decoder, 1024 symbols, 1 iteration
ber_atan2 = [0.4008789 0.395345 0.3892008 0.38826497 0.38407389 0.3815104 ...
             0.361857 0.35437 0.35734 0.341227 0.3011 0.2744 0.2594 0.2064 ...
             0.1556 0.1167 0.0845 0.0636 ...
             0.0360 0.0348 0.0269 0.0145 0.0143 0.0132 0.0103 0.0021];

% Mean
ber_mean = [0.0431722005208333 0.0251057942708333 0.0196126302083333 0.01123046875 ...
            0.00850423177083333 0.00394694010416667 0.00248209635416667 ...
            0.00142415364583333 0.0006103515625 0.000244140625 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ];

% Median
ber_median = [0.0775960286458333 0.0513509114583333 0.04248046875 0.0272216796875 ...
              0.01953125 0.0116780598958333 0.00870768229166667  0.00447591145833333 ...
              0.002685546875 0.00179036458333333 0.000691731770833333 0.000244140625 ...
              0.0001220703125   0 0 0 0 0 0 0 0 0 0 0 0 0 ];
% Weiszfeld
ber_Weisz = [ 0.058349609375 0.0369466145833333 0.0299886067708333 0.020751953125 ...
              0.0133463541666667 0.007080078125 0.00545247395833333 0.00297037760416667 ...
              0.00203450520833333 0.000244140625 0.000284830729166667 ...
              0 4.06901041666667e-05 0 0 0 0 0 0 0 0 0 0 0 0 0];

% Sq estimator
ber_S_q_99 = [0.0621744791666667 0.0455729166666667 0.033447265625 0.0238444010416667 ...
             0.0158284505208333 0.00874837239583333 0.0064697265625 0.00390625 ...
             0.00162760416666667 0.0009765625 0.000244140625 8.13802083333333e-05 ...
             0 8.13802083333333e-05 0 0 0 0 0 0 0 0 0 0 0 0];

% HQAM-MBM, Fig. 11
ber_HQAM_MBM = [.09 .042 .018 .0067 .0018 .0003];

% Theoretical QAM-MBM, Fig. 8
ber_QAM_MBM = [.8 .51 .27 .11 .04 .013];
snr_rng_QAM_MBM = 0:2:10;


% SNR range
snr_rng = -15:10;

% Font and marker sizes
font_size = 40;
marker_size = 20;
line_width = 3;

% Plot BER
f86 = figure(86);
%set(f86,'defaulttextinterpreter','latex');


set(0, 'DefaultTextFontName', 'Calibri', 'DefaultTextFontSize', font_size, ...
       'DefaultAxesFontName', 'Calibri', 'DefaultAxesFontSize', font_size); 

semilogy(snr_rng,ber_atan2,'s-','LineWidth',line_width,'MarkerSize',marker_size),hold,grid
semilogy(snr_rng,ber_mean,'d-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ber_median,'o-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ber_Weisz,'+-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ber_S_q_99,'*-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng_QAM_MBM,ber_HQAM_MBM,'mv-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng_QAM_MBM,ber_QAM_MBM,'c^-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ber_64_qam,'r--','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ber_8_psk,'k-','LineWidth',line_width,'MarkerSize',marker_size),hold

xlabel('SNR [dB]')
ylabel('Bit Error Rate')
legend('Atan2-Median','Mean','Median','Weiszfeld','Sq with q=0.99','HQAM-MBM','QAM-MBM','64-QAM','8-PSK')
       %'location','northoutside','Orientation','horizontal','NumColumns',5)

xlim([-15 10])
ylim([1e-5 1])

% Plot BER
f87 = figure(87);
%set(f87,'defaulttextinterpreter','latex');

set(0, 'DefaultTextFontName', 'Calibri', 'DefaultTextFontSize', font_size, ...
       'DefaultAxesFontName', 'Calibri', 'DefaultAxesFontSize', font_size); 

semilogy(snr_rng,ber_CReLU,'s-','LineWidth',line_width,'MarkerSize',marker_size),hold,grid
semilogy(snr_rng,ber_ModReLU_b_02,'+-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ber_ModReLU_b_05,'*-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ber_ModReLU_b_08,'d-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ber_Tanh,'o-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ber_HardTanh,'p-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ber_Asinh,'.-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ber_Casinh,'x-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng_QAM_MBM,ber_HQAM_MBM,'mv-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng_QAM_MBM,ber_QAM_MBM,'c^-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ber_64_qam,'r--','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ber_8_psk,'k-','LineWidth',line_width,'MarkerSize',marker_size),hold

%axis square
xlabel('SNR [dB]')
ylabel('Bit Error Rate')
legend('CReLU','ModReLU b=0.2','ModReLU b=0.5',...
       'ModReLU b=0.8','Tanh','HardTanh','Asinh','Casinh','HQAM-MBM','QAM-MBM')
       %'location','northoutside','Orientation','horizontal','NumColumns',5)

xlim([-15 10])
ylim([1e-5 1])

% EOF
