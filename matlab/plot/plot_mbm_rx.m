%
% Media-based Modulation with Reconfigurable Intelligent Surfaces
%
% Anders Buvarp
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%
%
% Input:
%
%    cfg            The configuration
%    rx             PxK samples of I/Q
%
function plot_mbm_rx(cfg,rx)

% Font and marker sizes
font_size = 30;
marker_size = 10;
line_width = 1;

f37 = figure(37);
set(f37,'defaulttextinterpreter','latex');

set(0, 'DefaultTextFontName', 'Calibri', 'DefaultTextFontSize', font_size, ...
       'DefaultAxesFontName', 'Calibri', 'DefaultAxesFontSize', font_size); 

eis = cfg.eisen;
re = real(eis);
im = imag(eis);
plot(re,im,'ko','LineWidth',line_width,'MarkerSize',marker_size)
hold,grid
plot(real(rx),imag(rx),'rx','LineWidth',line_width,'MarkerSize',marker_size)
hold

str = sprintf('Received at %d dB',cfg.snr_range(end));
legend('Transmit',str)
xlabel('Real')
ylabel('Imag')

% Re-size
%set(gcf,'units','points','position',[100,100,500,300])

% EOF
