%
% Plot symbol error rate (SER) for media-based modulation
%
% Anders Buvarp
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%

% Load data
fdCReLU = fopen('../accuracy/ser_CReLU.txt','r');
fdModReLUb02 = fopen('../accuracy/ser_ModReLU_b_02.txt','r');
fdModReLUb05 = fopen('../accuracy/ser_ModReLU_b_05.txt','r');
fdModReLUb08 = fopen('../accuracy/ser_ModReLU_b_08.txt','r');
fdAsinh = fopen('../accuracy/ser_Asinh.txt','r');
fdCasinh = fopen('../accuracy/ser_Casinh.txt','r');
fdTanh = fopen('../accuracy/ser_Tanh.txt','r');
fdHardTanh = fopen('../accuracy/ser_HardTanh.txt','r');

% Read BER data from files
ser_CReLU = fscanf(fdCReLU,'%f');
ser_ModReLU_b_02 = fscanf(fdModReLUb02,'%f');
ser_ModReLU_b_05 = fscanf(fdModReLUb05,'%f');
ser_ModReLU_b_08 = fscanf(fdModReLUb08,'%f');
ser_Asinh = fscanf(fdAsinh,'%f');
ser_Casinh = fscanf(fdCasinh,'%f');
ser_Tanh = fscanf(fdTanh,'%f');
ser_HardTanh = fscanf(fdHardTanh,'%f');
fclose(fdCReLU);fclose(fdModReLUb02);fclose(fdModReLUb05);fclose(fdModReLUb08);
fclose(fdAsinh);fclose(fdCasinh);fclose(fdTanh);fclose(fdHardTanh);

% 64-QAM SER
ser_64_qam = [0.90925      0.89763      0.88312       0.8649      0.84533       0.8145      0.78695      0.75062      0.71035 ...
              0.66087      0.60757      0.54842      0.47447       0.4058      0.32697      0.25613      0.18487      0.12443 ...
              0.078533     0.043367     0.020233       0.0087    0.0023833       0.0007   0.00011667   1.6667e-05];

% 8-PSK SER
ser_8_psk = [0.83163 0.82768 0.81965 0.81327 0.80915 0.79367 0.78622 0.77017 0.75712 ...
             0.74452 0.72428 0.70197 0.67392 0.64527 0.61443 0.57998 0.53333  0.4936 ...
             0.44245  0.3919 0.33643 0.28047 0.22413 0.17505  0.1269  0.0871];


% Arctan decoder, 1024 symbols, 1 iteration
ser_atan2 = [0.9047852 0.901855 0.89819 0.890869 0.873779 0.8515625 0.818115 ...
             0.79541   0.771973 0.74023 0.6846 0.6211 0.5488 0.4551 0.3887 ...
             0.2930 0.2266 0.1924 ...
             0.1045 0.0986 0.0723 0.0439 0.0430 0.0361 0.0303 0.0127];

% Mean
ser_mean = [0.134765625 0.078125 0.0634765625 0.03759765625 0.0263671875 ...
            0.01171875 0.008544921875 0.0048828125 0.002197265625 ...
            0.00048828125 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ];

% Median
ser_median = [0.235107421875 0.159423828125  0.13525390625 0.0869140625 0.059326171875 ...
              0.03857421875 0.02734375 0.01513671875 0.00830078125 0.005126953125 ...
              0.002197265625 0.00048828125 0.00048828125 0 0 0 0 0 0 0 0 0 0 0 0 0 ];

% Weiszfeld
ser_Weisz = [0.1806640625 0.11376953125 0.09912109375 0.065673828125 0.041748046875 ...
             0.023193359375 0.017333984375 0.009765625 0.00634765625 0.0009765625 ...
             0.001220703125 0 0.000244140625 0 0 0 0 0 0 0 0 0 0 0 0 0];

% Sq estimator
ser_S_q_99 = [0.19091796875 0.1435546875 0.1025390625 0.076171875 0.05224609375 ...
             0.02734375 0.019775390625 0.011962890625 0.005126953125 ...
             0.00244140625 0.0009765625 0.000244140625 0 0.000244140625 0 0 0 0 0 0 0 0 0 0 0 0];

% SNR range
snr_rng = -15:10;

% Font and marker sizes
font_size = 40;
marker_size = 20;
line_width = 3;

% Plot SER
f86 = figure(86);
%set(f86,'defaulttextinterpreter','latex');
set(0, 'DefaultTextFontName', 'Calibri', 'DefaultTextFontSize', font_size, ...
       'DefaultAxesFontName', 'Calibri', 'DefaultAxesFontSize', font_size); 

semilogy(snr_rng,ser_atan2,'s-','LineWidth',line_width,'MarkerSize',marker_size),hold,grid
semilogy(snr_rng,ser_mean,'d-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ser_median,'o-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ser_Weisz,'+-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ser_S_q_99,'*-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ser_64_qam,'r--','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ser_8_psk,'k-','LineWidth',line_width,'MarkerSize',marker_size),hold

xlabel('SNR [dB]')
ylabel('Symbol Error Rate')
legend('Atan2-Median','Mean','Median','Weiszfeld','Sq with q=0.99','64-QAM','8-PSK')

xlim([-15 10])
ylim([2e-4 1])

% Plot SER
f87 = figure(87);
%set(f87,'defaulttextinterpreter','latex');
set(0, 'DefaultTextFontName', 'Calibri', 'DefaultTextFontSize', font_size, ...
       'DefaultAxesFontName', 'Calibri', 'DefaultAxesFontSize', font_size); 

semilogy(snr_rng,ser_CReLU,'s-','LineWidth',line_width,'MarkerSize',marker_size),hold,grid
semilogy(snr_rng,ser_ModReLU_b_02,'+-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ser_ModReLU_b_05,'*-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ser_ModReLU_b_08,'d-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ser_Tanh,'o-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ser_HardTanh,'p-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ser_Asinh,'.-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ser_Casinh,'x-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ser_64_qam,'r--','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ser_8_psk,'k-','LineWidth',line_width,'MarkerSize',marker_size),hold

xlabel('SNR [dB]')
ylabel('Symbol Error Rate')
legend('CReLU','ModReLU b=0.2','ModReLU b=0.5','ModReLU b=0.8',...
       'Tanh','HardTanh','Asinh','Casinh','64-QAM','8-PSK')

xlim([-15 10])
ylim([2e-4 1])

% EOF

