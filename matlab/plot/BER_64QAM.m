M = 64; % Alphabet size
P = 8;
bitsPerSym = log2(M);
N = 20*60000; % Number of bits
x = randi([0 1],N,1);
txSig = pskmod(x,P, 'InputType', 'bit');
txSig = qammod(x,M, 'InputType', 'bit');
snr_range = -15:10;
ber = [];
for snr = snr_range%(end)
    rxSig = awgn(txSig,snr);
    %cd = comm.ConstellationDiagram(ShowReferenceConstellation=false);
    %cd(rxSig)
    z = pskdemod(rxSig,P, 'OutputType', 'bit');%,'PlotConstellation', true);
    z = qamdemod(rxSig,M, 'OutputType', 'bit');%,'PlotConstellation', true);
    ber = [ber sum(sum(x~=z))/N];
    %[num,rt]= symerr(x,z);
end % SNR

semilogy(snr_range,ber);grid;
title('Bit error rate for 64-QAM over AWGN');
xlabel('SNR [dB]');
ylabel('BER');
