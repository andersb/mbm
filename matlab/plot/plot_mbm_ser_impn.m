%
% Plot symbol error rate (SER) for media-based modulation
%
% Anders Buvarp
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%

% Load data
fdCReLU = fopen('../accuracy/ser_CReLU.txt','r');
fdModReLUb02 = fopen('../accuracy/ser_ModReLU_b_02_orate_1.txt','r');
fdModReLUb05 = fopen('../accuracy/ser_ModReLU_b_05_orate_1.txt','r');
fdModReLUb08 = fopen('../accuracy/ser_ModReLU_b_08.txt','r');
fdAsinh = fopen('../accuracy/ser_Asinh.txt','r');
fdCasinh = fopen('../accuracy/ser_Casinh.txt','r');
fdTanh = fopen('../accuracy/ser_Tanh_orate_1.txt','r');
fdHardTanh = fopen('../accuracy/ser_HardTanh.txt','r');

% Read BER data from files
ser_CReLU = fscanf(fdCReLU,'%f');
ser_ModReLU_b_02 = fscanf(fdModReLUb02,'%f');
ser_ModReLU_b_05 = fscanf(fdModReLUb05,'%f');
ser_ModReLU_b_08 = fscanf(fdModReLUb08,'%f');
ser_Asinh = fscanf(fdAsinh,'%f');
ser_Casinh = fscanf(fdCasinh,'%f');
ser_Tanh = fscanf(fdTanh,'%f');
ser_HardTanh = fscanf(fdHardTanh,'%f');
fclose(fdCReLU);fclose(fdModReLUb02);fclose(fdModReLUb05);fclose(fdModReLUb08);
fclose(fdAsinh);fclose(fdCasinh);fclose(fdTanh);fclose(fdHardTanh);

% 64-QAM SER
ser_64_qam = [0.90925      0.89763      0.88312       0.8649      0.84533       0.8145      0.78695      0.75062      0.71035 ...
              0.66087      0.60757      0.54842      0.47447       0.4058      0.32697      0.25613      0.18487      0.12443 ...
              0.078533     0.043367     0.020233       0.0087    0.0023833       0.0007   0.00011667   1.6667e-05];

% 8-PSK SER
ser_8_psk = [0.83163 0.82768 0.81965 0.81327 0.80915 0.79367 0.78622 0.77017 0.75712 ...
             0.74452 0.72428 0.70197 0.67392 0.64527 0.61443 0.57998 0.53333  0.4936 ...
             0.44245  0.3919 0.33643 0.28047 0.22413 0.17505  0.1269  0.0871];

% Arctan decoder, 1024 symbols, 1 iteration
ser_atan2_in = [0.906005859 0.902099609 0.900635 0.889648 0.875488 ...
                0.853027 0.8188477 0.7973633 0.7746582 0.7446289 ...
                0.6899414 0.621826 0.5615234 0.4858398 0.3928223 ...
                0.2875977 0.226318 0.1943359 0.1228027 0.107666 ...
                0.0878906 0.062012 0.0598145 0.0390625 0.031982421875 ...
                0.016357421875];

% Mean
ser_mean_in = [0.27587890625 0.205078125 0.157958984375 0.127685546875 ...
               0.092041015625 0.0625 0.04296875 0.029541015625 0.023681640625 ...
               0.009765625 0.005859375 0.002197265625 0.0009765625 0.00048828125 ...
               0.00048828125 0 0 0 0 0 0 0 0 0 0 0];

% Median
ser_median_in = [0.218017578125 0.204833984375 0.120849609375 0.094970703125 ...
                 0.0712890625 0.045166015625 0.025634765625 0.01513671875 ...
                 0.011474609375 0.00537109375 0.0029296875 0.000732421875 ...
                 0.000244140625 0 0 0 0 0 0 0 0 0 0 0 0 0];

% Weiszfeld
ser_Weisz_in = [0.171875 0.15087890625 0.0888671875 0.066162109375 0.0537109375 ...
                0.027099609375 0.015380859375 0.01025390625 0.00732421875 ...
                0.00146484375 0.001708984375 0.00048828125 0.000244140625 ...
                0 0 0 0 0 0 0 0 0 0 0 0 0];

% Sq estimator
ser_S_q_99_in = [0.171875 0.140625 0.10302734375 0.05908203125 0.04443359375 ...
                 0.02685546875 0.013671875 0.0087890625 0.0078125 0.00146484375 ...
                 0.00048828125 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];

% SNR range
rob_snr_rng = -15:10;

% Font and marker sizes
font_size = 40;
marker_size = 20;
line_width = 3;

% Plot SER with IMPN
f90 = figure(90);
%set(f90,'defaulttextinterpreter','latex');
set(0, 'DefaultTextFontName', 'Calibri', 'DefaultTextFontSize', font_size, ...
       'DefaultAxesFontName', 'Calibri', 'DefaultAxesFontSize', font_size); 

semilogy(rob_snr_rng,ser_atan2_in,'s-','LineWidth',line_width,'MarkerSize',marker_size),hold,grid
semilogy(rob_snr_rng,ser_mean_in,'d-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(rob_snr_rng,ser_median_in,'o-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(rob_snr_rng,ser_Weisz_in,'+-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(rob_snr_rng,ser_S_q_99_in,'*-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(rob_snr_rng,ser_64_qam,'r--','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(rob_snr_rng,ser_8_psk,'k-','LineWidth',line_width,'MarkerSize',marker_size),hold

xlabel('SNR [dB]')
ylabel('Symbol Error Rate')
legend('Atan2-Median','Mean','Median','Weiszfeld','Sq with q=0.99','64-QAM','8-PSK')

xlim([-15 10])
ylim([2e-4 1])

% Plot SER with IMPN
f91 = figure(91);
%set(f91,'defaulttextinterpreter','latex');
set(0, 'DefaultTextFontName', 'Calibri', 'DefaultTextFontSize', font_size, ...
       'DefaultAxesFontName', 'Calibri', 'DefaultAxesFontSize', font_size); 

semilogy(rob_snr_rng,ser_CReLU,'s-','LineWidth',line_width,'MarkerSize',marker_size),hold,grid
semilogy(rob_snr_rng,ser_ModReLU_b_02,'+-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(rob_snr_rng,ser_ModReLU_b_05,'*-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(rob_snr_rng,ser_ModReLU_b_08,'d-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(rob_snr_rng,ser_Tanh,'o-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(rob_snr_rng,ser_HardTanh,'p-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(rob_snr_rng,ser_Asinh,'.-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(rob_snr_rng,ser_Casinh,'x-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(rob_snr_rng,ser_64_qam,'r--.','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(rob_snr_rng,ser_8_psk,'k-','LineWidth',line_width,'MarkerSize',marker_size),hold

xlabel('SNR [dB]')
ylabel('Symbol Error Rate')
legend('CReLU','ModReLU b=0.2','ModReLU b=0.5','ModReLU b=0.8',...
       'Tanh','HardTanh','Asinh','Casinh','64-QAM','8-PSK')

xlim([-15 10])
ylim([2e-4 1])

% EOF
