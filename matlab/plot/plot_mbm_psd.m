%
% Plot PSD for media-based modulation
%
% Anders Buvarp
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%

function plot_mbm_psd(F,Pxx)

% Font and marker sizes
font_size = 40;
marker_size = 20;

figure(41);
set(0, 'DefaultTextFontName', 'Calibri', 'DefaultTextFontSize', font_size, ...
       'DefaultAxesFontName', 'Calibri', 'DefaultAxesFontSize', font_size); 

% Heat map

surf(1:size(Pxx,2),F/1e9,10*log10(abs(Pxx)),'EdgeColor','none');

set(gca,'YDir','normal');
colormap turbo
cb = colorbar('eastoutside')
p = cb.Position;
set(cb,'Position',[p(1)+.08 p(2)-.0 .02 .8])

ylabel(cb,'Power (dBm)','FontSize',font_size,'Rotation',270);
lbpos = get(cb,'ylabel');
pos = get(lbpos,'position'); 
pos(1) = pos(1) - 5;
set(lbpos, 'position', pos);
xlabel('Symbols')
ylabel('Freq. [GHz]')    
zlabel('Power')

% EOF
