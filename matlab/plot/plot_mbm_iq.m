%
% Plot the received MBM time slot
%
% Anders Buvarp
%
% Virginia Tech National Security Institute
%
%
%
% Input:
%
%    rx             The received I/Q samples
%
function plot_mbm_rx(rx,id)

% Font and marker sizes
font_size = 30;
marker_size = 1;
line_width = 1;

figure(id)
set(0, 'DefaultTextFontName', 'Calibri', 'DefaultTextFontSize', font_size, ...
       'DefaultAxesFontName', 'Calibri', 'DefaultAxesFontSize', font_size); 

re = real(rx);
im = imag(rx);

v = [re im];
ma = max(v);
mi = min(v);
li = [mi ma];

subplot(211)
plot(re,'LineWidth',line_width,'MarkerSize',marker_size)
grid
ylim(li)
ylabel('real')

subplot(212)
plot(im,'LineWidth',line_width,'MarkerSize',marker_size)
grid
ylim(li)
ylabel('imag')


% EOF
