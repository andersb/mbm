%
% Plot BER/SER for LOS.
%
% Anders Buvarp
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%

% Load BER data
fdCReLU = fopen('../accuracy/ber_CReLU_orate_1_los.txt','r');
fdModReLUb02 = fopen('../accuracy/ber_ModReLU_b_02_orate_1_los.txt','r');
fdModReLUb05 = fopen('../accuracy/ber_ModReLU_b_05_orate_1_los.txt','r');
fdModReLUb08 = fopen('../accuracy/ber_ModReLU_b_08_orate_1_los.txt','r');
fdAsinh = fopen('../accuracy/ber_Asinh_orate_1_los.txt','r');
fdCasinh = fopen('../accuracy/ber_Casinh_orate_1_los.txt','r');
fdTanh = fopen('../accuracy/ber_Tanh_orate_1_los.txt','r');
fdHardTanh = fopen('../accuracy/ber_HardTanh_orate_1_los.txt','r');

% Read BER data from files
ber_CReLU = fscanf(fdCReLU,'%f');
ber_ModReLU_b_02 = fscanf(fdModReLUb02,'%f');
ber_ModReLU_b_05 = fscanf(fdModReLUb05,'%f');
ber_ModReLU_b_08 = fscanf(fdModReLUb08,'%f');
ber_Asinh = fscanf(fdAsinh,'%f');
ber_Casinh = fscanf(fdCasinh,'%f');
ber_Tanh = fscanf(fdTanh,'%f');
ber_HardTanh = fscanf(fdHardTanh,'%f');

% Close files
fclose(fdCReLU);fclose(fdModReLUb02);fclose(fdModReLUb05);fclose(fdModReLUb08);
fclose(fdAsinh);fclose(fdCasinh);fclose(fdTanh);fclose(fdHardTanh);

% 64-QAM BER
ber_64_qam = [0.34008 0.32001 0.29911 0.27818 0.25973 0.23382 0.21216 ...
              0.19096 0.16886 0.14833 0.12893 0.11133 0.093542 0.075842 ...
              0.060883 0.045742 0.032667 0.022542 0.013067 0.0076333 ...
              0.0035167 0.0013933 0.00048 0.00010083 1.9167e-05 2.5e-06];

% 8-PSK BER
ber_8_psk = [0.45451 0.44881 0.44133 0.43501 0.42536 0.41424 0.40597  0.3952 0.38244 ...
            0.36495 0.35065 0.33008 0.31121 0.29021 0.26687 0.24135 0.21675 0.19019 ...
            0.16411 0.14147 0.11777  0.0976 0.076992 0.059225 0.04255 0.028958];

% SNR range
snr_rng = -15:10;

% Font and marker sizes
font_size = 40;
marker_size = 20;
line_width = 3;

% Plot BER with IMPN for LOS
f94 = figure(94);
%set(f94,'defaulttextinterpreter','latex');
set(0, 'DefaultTextFontName', 'Calibri', 'DefaultTextFontSize', font_size, ...
       'DefaultAxesFontName', 'Calibri', 'DefaultAxesFontSize', font_size); 

semilogy(snr_rng,ber_CReLU,'s-','LineWidth',line_width,'MarkerSize',marker_size),hold,grid
semilogy(snr_rng,ber_ModReLU_b_02,'+-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ber_ModReLU_b_05,'*-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ber_ModReLU_b_08,'d-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ber_Tanh,'o-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ber_HardTanh,'p-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ber_Asinh,'.-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ber_Casinh,'x-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ber_64_qam,'r--','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ber_8_psk,'k-','LineWidth',line_width,'MarkerSize',marker_size),hold

xlabel('SNR [dB]')
ylabel('Bit Error Rate')
legend('CReLU','ModReLU b=0.2','ModReLU b=0.5',...
       'ModReLU b=0.8','Tanh','HardTanh','Asinh','Casinh','64-QAM','8-PSK')

xlim([-15 10])
ylim([3e-5 1])

% Load SER data
fdCReLU = fopen('../accuracy/ser_CReLU_orate_1_los.txt','r');
fdModReLUb02 = fopen('../accuracy/ser_ModReLU_b_02_orate_1_los.txt','r');
fdModReLUb05 = fopen('../accuracy/ser_ModReLU_b_05_orate_1_los.txt','r');
fdModReLUb08 = fopen('../accuracy/ser_ModReLU_b_08_orate_1_los.txt','r');
fdAsinh = fopen('../accuracy/ser_Asinh_orate_1_los.txt','r');
fdCasinh = fopen('../accuracy/ser_Casinh_orate_1_los.txt','r');
fdTanh = fopen('../accuracy/ser_Tanh_orate_1_los.txt','r');
fdHardTanh = fopen('../accuracy/ser_HardTanh_orate_1_los.txt','r');

% Read SER data from files
ser_CReLU = fscanf(fdCReLU,'%f');
ser_ModReLU_b_02 = fscanf(fdModReLUb02,'%f');
ser_ModReLU_b_05 = fscanf(fdModReLUb05,'%f');
ser_ModReLU_b_08 = fscanf(fdModReLUb08,'%f');
ser_Asinh = fscanf(fdAsinh,'%f');
ser_Casinh = fscanf(fdCasinh,'%f');
ser_Tanh = fscanf(fdTanh,'%f');
ser_HardTanh = fscanf(fdHardTanh,'%f');

% Close files
fclose(fdCReLU);fclose(fdModReLUb02);fclose(fdModReLUb05);fclose(fdModReLUb08);
fclose(fdAsinh);fclose(fdCasinh);fclose(fdTanh);fclose(fdHardTanh);

% 64-QAM SER
ser_64_qam = [0.90925      0.89763      0.88312       0.8649      0.84533       0.8145      0.78695      0.75062      0.71035 ...
              0.66087      0.60757      0.54842      0.47447       0.4058      0.32697      0.25613      0.18487      0.12443 ...
              0.078533     0.043367     0.020233       0.0087    0.0023833       0.0007   0.00011667   1.6667e-05];

% 8-PSK SER
ser_8_psk = [0.83163 0.82768 0.81965 0.81327 0.80915 0.79367 0.78622 0.77017 0.75712 ...
             0.74452 0.72428 0.70197 0.67392 0.64527 0.61443 0.57998 0.53333  0.4936 ...
             0.44245  0.3919 0.33643 0.28047 0.22413 0.17505  0.1269  0.0871];

% Plot SER with IMPN for LOS
f95 = figure(95);
%set(f95,'defaulttextinterpreter','latex');
set(0, 'DefaultTextFontName', 'Calibri', 'DefaultTextFontSize', font_size, ...
       'DefaultAxesFontName', 'Calibri', 'DefaultAxesFontSize', font_size); 

semilogy(snr_rng,ser_CReLU,'s-','LineWidth',line_width,'MarkerSize',marker_size),hold,grid
semilogy(snr_rng,ser_ModReLU_b_02,'+-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ser_ModReLU_b_05,'*-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ser_ModReLU_b_08,'d-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ser_Tanh,'o-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ser_HardTanh,'p-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ser_Asinh,'.-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ser_Casinh,'x-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ser_64_qam,'r--','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(snr_rng,ser_8_psk,'k-','LineWidth',line_width,'MarkerSize',marker_size),hold

xlabel('SNR [dB]')
ylabel('Symbol Error Rate')
legend('CReLU','ModReLU b=0.2','ModReLU b=0.5','ModReLU b=0.8',...
       'Tanh','HardTanh','Asinh','Casinh','64-QAM','8-PSK')

xlim([-15 10])
ylim([2e-4 1])


% EOF
