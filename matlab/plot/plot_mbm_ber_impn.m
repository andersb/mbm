%
% Plot bit error rate (BER) for media-based modulation
%
% Anders Buvarp
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%

% Load data
fdCReLU = fopen('../accuracy/ber_CReLU_orate_1.txt','r');
fdModReLUb02 = fopen('../accuracy/ber_ModReLU_b_02_orate_1.txt','r');
fdModReLUb05 = fopen('../accuracy/ber_ModReLU_b_05_orate_1.txt','r');
fdModReLUb08 = fopen('../accuracy/ber_ModReLU_b_08_orate_1.txt','r');
fdAsinh = fopen('../accuracy/ber_Asinh_orate_1.txt','r');
fdCasinh = fopen('../accuracy/ber_Casinh_orate_1.txt','r');
fdTanh = fopen('../accuracy/ber_Tanh_orate_1.txt','r');
fdHardTanh = fopen('../accuracy/ber_HardTanh_orate_1.txt','r');

% Read BER data from files
ber_CReLU = fscanf(fdCReLU,'%f');
ber_ModReLU_b_02 = fscanf(fdModReLUb02,'%f');
ber_ModReLU_b_05 = fscanf(fdModReLUb05,'%f');
ber_ModReLU_b_08 = fscanf(fdModReLUb08,'%f');
ber_Asinh = fscanf(fdAsinh,'%f');
ber_Casinh = fscanf(fdCasinh,'%f');
ber_Tanh = fscanf(fdTanh,'%f');
ber_HardTanh = fscanf(fdHardTanh,'%f');

fclose(fdCReLU);fclose(fdModReLUb02);fclose(fdModReLUb05);fclose(fdModReLUb08);
fclose(fdAsinh);fclose(fdCasinh);fclose(fdTanh);fclose(fdHardTanh);

% 64-QAM BER
ber_64_qam = [0.34008 0.32001 0.29911 0.27818 0.25973 0.23382 0.21216 ...
              0.19096 0.16886 0.14833 0.12893 0.11133 0.093542 0.075842 ...
              0.060883 0.045742 0.032667 0.022542 0.013067 0.0076333 ...
              0.0035167 0.0013933 0.00048 0.00010083 1.9167e-05 2.5e-06];

% 8-PSK BER
ber_8_psk = [0.45451 0.44881 0.44133 0.43501 0.42536 0.41424 0.40597  0.3952 0.38244 ...
            0.36495 0.35065 0.33008 0.31121 0.29021 0.26687 0.24135 0.21675 0.19019 ...
            0.16411 0.14147 0.11777  0.0976 0.076992 0.059225 0.04255 0.028958];

% Arctan decoder, 1024 symbols, 1 iteration
ber_atan2_in = [0.400634765625 0.396077473958 0.39107259 0.3880208 ...
                0.3838297526 0.38077799479 0.3638509 0.353759765625 ...
                0.35791015625 0.3433024 0.290242513 0.265258789 ...
                0.2498779296875 0.215413411458333 ...
                0.165120443 0.1205647786 0.0870361 0.0649007161458333 ...
                0.038370768 0.034505208 0.0291748 0.017985026 0.01700846 ...
                0.013671875 0.010498 0.0028076171875];

% Mean
ber_mean_in = [0.0978597005208333 0.0723876953125 0.0572509765625 0.0436197916666667 ...
               0.0302327473958333 0.022216796875  0.015380859375 0.0102132161458333 ...
               0.00773111979166667 0.00374348958333333 0.00223795572916667 ...
               0.000935872395833333 0.0003662109375 8.13802083333333e-05 ...
               0.000325520833333333 0 0 0 0 0 0 0 0 0 0 0];

% Median
ber_median_in = [0.075927734375 0.0701904296875 0.042724609375 0.0334065755208333 ...
                 0.0259602864583333 0.0169270833333333, 0.00834147135416667 ...
                 0.00565592447916667 0.00431315104166667 0.00215657552083333 ...
                 0.001220703125 0.0003662109375 4.06901041666667e-05 ...
                 0 0 0 0 0 0 0 0 0 0 0 0 0];

% Weiszfeld
ber_Weisz_in = [0.0605875651041667 0.0520426432291667 0.0316569010416667 ...
                0.0226643880208333 0.0194905598958333 0.0103352864583333 ...
                0.005859375 0.0037841796875 0.00260416666666667 ...
                0.000569661458333333 0.000773111979166667 0.000284830729166667 ...
                4.06901041666667e-05  0 0 0 0 0  0 0 0 0 0 0 0 0];

% Sq estimator
ber_S_q_99_in = [0.108235677 0.09073893 0.0772298 0.057047526 0.037638 0.0287272 ...
             0.02115885 0.013712565 0.007446289 0.004435221 0.0029703776 ...
             0.00101725 0.00065104 0.00032552 0.00032552 0 0 0 0 0 0 0 0 0 0 0];

% SNR range
rob_snr_rng = -15:10;

% Font and marker sizes
font_size = 40;
marker_size = 20;
line_width = 3;

% Plot BER with IMPN
f88 = figure(88);
%set(f88,'defaulttextinterpreter','latex');
set(0, 'DefaultTextFontName', 'Calibri', 'DefaultTextFontSize', font_size, ...
       'DefaultAxesFontName', 'Calibri', 'DefaultAxesFontSize', font_size); 

semilogy(rob_snr_rng,ber_atan2_in,'s-','LineWidth',line_width,'MarkerSize',marker_size),hold,grid
semilogy(rob_snr_rng,ber_mean_in,'d-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(rob_snr_rng,ber_median_in,'o-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(rob_snr_rng,ber_Weisz_in,'+-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(rob_snr_rng,ber_S_q_99_in,'*-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(rob_snr_rng,ber_64_qam,'r--','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(rob_snr_rng,ber_8_psk,'k-','LineWidth',line_width,'MarkerSize',marker_size),hold

xlabel('SNR [dB]')
ylabel('Bit Error Rate')
legend('Atan2-Median','Mean','Median','Weiszfeld','Sq with q=0.99','64-QAM','8-PSK')

xlim([-15 10])
ylim([3e-5 1])


% Plot BER with IMPN
f89 = figure(89);
%set(f89,'defaulttextinterpreter','latex');
set(0, 'DefaultTextFontName', 'Calibri', 'DefaultTextFontSize', font_size, ...
       'DefaultAxesFontName', 'Calibri', 'DefaultAxesFontSize', font_size); 

semilogy(rob_snr_rng,ber_CReLU,'S-','LineWidth',line_width,'MarkerSize',marker_size),hold,grid
semilogy(rob_snr_rng,ber_ModReLU_b_02,'+-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(rob_snr_rng,ber_ModReLU_b_05,'*-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(rob_snr_rng,ber_ModReLU_b_08,'d-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(rob_snr_rng,ber_Tanh,'o-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(rob_snr_rng,ber_HardTanh,'p-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(rob_snr_rng,ber_Asinh,'.-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(rob_snr_rng,ber_Casinh,'x-','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(rob_snr_rng,ber_64_qam,'r--','LineWidth',line_width,'MarkerSize',marker_size)
semilogy(rob_snr_rng,ber_8_psk,'k-','LineWidth',line_width,'MarkerSize',marker_size),hold

xlabel('SNR [dB]')
ylabel('Bit Error Rate')
legend('CReLU','ModReLU b=0.2','ModReLU b=0.5',...
       'ModReLU b=0.8','Tanh','HardTanh','Asinh','Casinh','64-QAM','8-PSK')

xlim([-15 10])
ylim([3e-5 1])


% EOF
