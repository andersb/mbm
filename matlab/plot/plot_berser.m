%
% Plot the GM iterations
%
% Anders Buvarp
%
% Virginia Tech National Security Institute
%
%
%
% Input:
%
%    rx             The received quaternions
%
function plot_berser()

% Font and marker sizes
font_size = 40;
marker_size = 1;
line_width = 4;
snr_rng = -15:-1;

f46 = figure(46);
set(0, 'DefaultTextFontName', 'Calibri', 'DefaultTextFontSize', font_size, ...
       'DefaultAxesFontName', 'Calibri', 'DefaultAxesFontSize', font_size); 
%set(f46,'defaulttextinterpreter','latex');

load berser_q_75_loc.mat
semilogy(snr_rng,ber,'LineWidth',line_width,'MarkerSize',marker_size)
grid,hold
load berser_q_80_loc.mat
semilogy(snr_rng,ber,'LineWidth',line_width,'MarkerSize',marker_size)
load berser_q_85_loc.mat
semilogy(snr_rng,ber,'LineWidth',line_width,'MarkerSize',marker_size)
load berser_q_90_loc.mat
semilogy(snr_rng,ber,'LineWidth',line_width,'MarkerSize',marker_size)
load berser_q_95_loc.mat
semilogy(snr_rng,ber,'LineWidth',line_width,'MarkerSize',marker_size)
load berser_q_99_loc.mat
semilogy(snr_rng,ber,'LineWidth',line_width,'MarkerSize',marker_size)
%load berser_q_75.mat
%semilogy(snr_rng,ber,'LineWidth',line_width,'MarkerSize',marker_size)
hold

xlabel('SNR [dB]')
ylabel('Bit Error Rate')
title('BER vs SNR for Sq-estimator')
legend('q=0.75','q=0.80','q=0.85','q=0.90','q=0.95','q=0.99')%,'full q=0.75')
xlim([-15 -3.5])
ylim([7e-5 .1])
% EOF
