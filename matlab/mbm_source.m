%
% Generate random data and carrier wave
%
% Anders Buvarp
%
% References:
%
%   Emil Bjornsson, et al., "Intelligent Reflecting Surface Operation Under 
%   Predictable Receiver Mobility: A Continuous Time Propagation Model", 
%   IEEE WIRELESS COMMUNICATIONS LETTERS, VOL. 10, NO. 2, 2021
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%
% Input:
%
%    cfg            Configuration
%    data           Payload
%
% Output:
%
%    slots          The time slot waves
%    los_slots      The time slot waves
%

function [slots,los_slots] = mbm_source(cfg,data)

% Generate transmission
num_samps = cfg.num_code_words * cfg.slot_len;
t = (0:num_samps)*cfg.Ts;
carrier = cfg.E0*exp(1j*cfg.Omega_c*t);

% Allocate the slots
slots = zeros(cfg.num_code_words,cfg.slot_len);
los_slots = slots;
kx = 1:cfg.slot_len;

% Loop for all code words
for sx = 1:cfg.num_code_words
    % Extract section of the carrier wave
    sig = carrier(kx);
    kx = kx + cfg.slot_len;
    % Get the phase of the code word
    dx = 1+data(sx);
    pdiff = cfg.eisen_phase(dx)+pi;
    if data(sx) == 51
        %pdiff = pdiff - 0.2;
        %pdiff = 0;
    end 
    % Shift the phase
    w = sig * exp(1j*pdiff);
    % Save the time slot
    slots(sx,:) = w;
    los_slots(sx,:) = sig;
end % for

% EOF


