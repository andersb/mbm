%
% AWGN Channel
%
% Anders Buvarp
%
%
% Virginia Tech National Security Institute
%
%
%
% Input:
%
%    cfg     Configuration
%    iq      I/Q samples
%    snr     Signal-to-Noise Ratio
%
% Output:
%
%    out     Channel output
%

function out = ch(cfg,iq,snr)

% Get signal power
[N,K] = size(iq);
sig_pwr = trace(iq*iq') / (N*K);

% Linear SNR
lin_snr = 10^(snr/10);

% Get noise power
cfg.noise_pwr = sig_pwr / lin_snr;

% Standard deviation
% 50% power each in I and Q
s = sqrt(cfg.noise_pwr/2);

% Generate noise
awgn = randn(N,K)+1j*randn(N,K);
awgn = s*awgn;

% Outlier index generation with 1% chance
p = randi(100,1,cfg.K);
ox = find(p <= cfg.outlier_rate);

% 1% chance of impulse noise
awgn(:,ox) = cfg.sigma_impn*awgn(:,ox);

% Channel output
out = iq + awgn;

% EOF