%
% Reconfigurable Intelligent Surfaces for polarization-space modulation
%
% Anders Buvarp
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%
% Input:
%
%   num_iter            Number of Monte-Carlo iterations
%   num_code_words      Number of code words
%   snr_rng             SNR range
%   num_snapshots       Number of snapshots
%
%
% Output:
%
%   cfg                 Configuration
%

function cfg = mbm_config(cfg,num_iter,num_code_words,snr_rng,num_snapshots)

% Set-up the Monte-Carlo configuration
cfg.num_iter = num_iter;
cfg.num_code_words = num_code_words;
cfg.symb_range = 1:num_code_words;
cfg.param_type = 'PSM';
cfg.snr_range = snr_rng;
cfg.num_snr = numel(snr_rng);

% Code word length [bits]
cfg.code_word_len = 6;

% Source magnitude
cfg.E0 = 1;                                     

% 5G-NR FR2, Band n260, K_a-band
cfg.Fc = 39e9;                              % Carrier frequency
cfg.c0 = 3e8;                               % Speed of light
cfg.wavelength = cfg.c0 / cfg.Fc;           % Wavelength
cfg.Omega_c = 2*pi*cfg.Fc;                  % Angular velocity

% Sample rate
cfg.over_sampling = 8;
cfg.Fs = cfg.Fc*cfg.over_sampling;
cfg.Ts = 1/cfg.Fs;

% Down-conversion vector
cfg.K = num_snapshots;
ix = 1:cfg.K;
ix = ix-1;
T = cfg.Ts*ix;
Fdc = 39e9;
ddc = exp(-2j*pi*Fdc*T);
cfg.ddc = ddc;

% EOF
