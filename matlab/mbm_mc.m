%
% Monte Carlo Model for MBM
%
% Anders Buvarp
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%
% Input:
%
%    cfg                        The configuration structure:
%    payload                    Payload
%
% Output:
%
%    ber                        Bit-error rate
%    ser                        Quaternions
%    iq                         I/Q
%
function [ber,ser,iq] = mbm_mc(cfg,payload)

% Monte-Carlo Configuration
num_iter = cfg.num_iter;
param_type = cfg.param_type;
symb_range = cfg.symb_range;
snr_range = cfg.snr_range;

if cfg.verbose

    %
    % Print Monte-Carlo simulation parameters
    %

    fprintf('\nTransmitter located at:\t\t\t\t(%.3f,%.3f)',cfg.p_tx(1),cfg.p_tx(2))
    fprintf('\nReceiver located at:\t\t\t\t(%.3f,%.3f)',cfg.p_rx(1),cfg.p_rx(2))
    fprintf('\nRIS azimuth:\t\t\t\t%.3f',cfg.ris_azimuth)
    fprintf('\nRIS dimensions:\t\t\t\t%d x %d elements',cfg.E.rows,cfg.E.cols)
    fprintf('\nRIS dimensions:\t\t\t\t%.3f x %.3f meters',cfg.ris_height,cfg.ris_width)
    fprintf('\nRIS spacing, d_y:\t\t\t%.3f [mm]',cfg.d_y*1000)
    fprintf('\nRIS spacing, d_z:\t\t\t%.3f [mm]',cfg.d_z*1000)
    fprintf('\nSNR Min:\t\t\t\t%d',snr_range(1))
    fprintf('\nSNR Max:\t\t\t\t%d',snr_range(end))

    fprintf('\nParam Type:\t\t\t\t%s',param_type)
    fprintf('\nParam Min:\t\t\t\t%.3f',symb_range(1))
    fprintf('\nParam Max:\t\t\t\t%.3f',symb_range(end))
    fprintf('\nSample Rate:\t\t\t\t%.3f [Msamples/s]',cfg.Fs/1e6)
    fprintf('\nSample Period:\t\t\t\t%.3f [nanoseconds]',cfg.Ts*1e9)
    fprintf('\nWavelength:\t\t\t\t%.3f [mm]',cfg.wavelength*1000)
    
    fprintf('\n#######################################################')
    fprintf('\nMonte Carlo with %d iterations for %d slots',num_iter,cfg.num_code_words)
    fprintf('\n#######################################################')
  
end % verbose

% BER and SER
s = length(snr_range);
ber = zeros(s,num_iter);
ser = ber;

% Save I/Q to MATLAB file
% SNR levels x iter x code words x ula x snapshots
iq = zeros(s,num_iter,cfg.num_code_words,cfg.ula_len,cfg.K);

% Generate time slots from the payload
[slots,los_slots] = mbm_source(cfg,payload);

% Payload bits
payload_bits = de2bi(payload);

% Store magnitudes and phases
a = length(symb_range);
mags = zeros(1,a);
phs = mags;

%dump = [];

tic
% Loop for range of SNR
sx = 1;
for snr = snr_range
    fprintf('\nSNR: %d dB',snr_range(sx))

    % Monte-Carlo iterations
    for count = 1:num_iter
        fprintf('\nIteration: %d',count)

        % Loop for symbols
        for symb = symb_range
        
            % Heart beat
            d = payload(symb);
            %if ~cfg.verbose && ~mod(symb-1,128)
            if ~mod(symb-1,128)
                % Print slot
                fprintf('\nSlot: %d\t\tData: %d',symb,d)
            end % if
          
            % Generate the snapshots
            slot = slots(symb,:);
            los_slot = los_slots(symb,:);
            ss = snapshots(cfg,slot,los_slot,d);
        
            % AWGN
            rx = ch(cfg,ss,snr);

            % Down-conversion to baseband
            bb = rx .* cfg.ddc;
            if 0
                r = reshape(rx,1,[]);
                r = decimate(r,8);
                win = hamming(80);
                Fs = 39e9;
                [Pxx(:,symb),F] = pwelch(r,win,length(win)/2,length(win),Fs,'centered','power');
            end % if
            
            if cfg.save_iq_flg
                % Save I/Q
                iq(sx,count,symb,:,:) = bb;
                continue
            end % if

            % Robust baseband estimation
            if cfg.robust_flg || cfg.Sq_flg
                gmed = Weiszfeld(cfg,bb);
                gmed_cv = gmed(1)+1j*gmed(2);
                if cfg.Sq_flg
                    % q-paramter
                    q = cfg.q_param;
                    % Initial location estimate
                    loc0 = cfg.all_ones .* gmed_cv;
                    % Initial scatter
                    scatter = bb*bb'./cfg.ula_len;
                    % Sq-estimator of location
                    [hat,~,~,d, status] = sEst(bb,'QGAUSS',[],q,loc0,scatter);
                    %dump = [dump;d];
                    % Estimate the received complex sample
                    re = real(hat);
                    im = imag(hat);
                    bb = median(re)+1j*median(im);
                    %delta = gmed_cv-bb
                else
                    bb = gmed_cv;
                end % if
            end % if

            if cfg.decode_flg

                % Phase
                re = real(bb);
                im = imag(bb);
                at = atan2(im,re);
                phs(symb) = median(median(at,2));
                
                % Magnitude
                ab = abs(bb);
                mags(symb) = median(median(ab,2));
                
            end % if cfg.decode_flg

        end % for each symbol

        if cfg.decode_flg

            data = decode_mbm(cfg,phs,mags);

            % Symbol errors
            ds = data ~= payload;
            serr = sum(sum(ds));
            
            % Bit errors
            db = de2bi(data,cfg.code_word_len) ~= payload_bits;
            berr = sum(sum(db));

            % Print decoded data and source
            if cfg.verbose
                %fprintf('\nSlot: %d\tSource: %d\t\tDecoded Data: %d\t\tNum Bit Error(s): %d',symb,payload,data,err)
            end % if

            % Save errors
            ber(sx,count) = berr;
            ser(sx,count) = serr;

            if 0
                % Print current BER
                num_bits = symb*num_iter*cfg.code_word_len;
                b = sum(ber,[2 3]) / num_bits;
                fprintf('\n#####################################################')
                cnt = numel(snr_range);
                for sx = 1:cnt
                    fprintf('\nSNR:\t%d\t\tBER:\t%.2e',snr_range(sx),b(sx))
                end % for
                fprintf('\n#####################################################')
            end % if
        end % if ~cfg.save_iq_flg
    end % for iter

    % Update the SNR index
    sx = sx+1;
end % for SNR

dur = toc / 60;
fprintf('\n#####################################################')
fprintf('\nThe Monte-Carlo simulation took %.3f minutes',dur)
fprintf('\n#####################################################')
fprintf('\n')

% EOF
